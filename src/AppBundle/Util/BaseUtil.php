<?php
/**
 * Created by PhpStorm.
 * User: testera
 * Date: 1/04/15
 * Time: 10:05
 */

namespace AppBundle\Util;

/**
 * Class BaseUtil
 *
 * Clase de utilidades genéricas.
 *
 * @package AppBundle\Util
 */
class BaseUtil {


    /**
     * Este método devuelve el formato (patrón) de fecha asociado a un locale determinado. Se puede especificar
     * el locale, pero si no se hace se utiliza el locale activo en el momento (será lo que se hará normalmente).
     *
     * También se puede especificar el formato deseado utilizando ctes IntlDateFormatter. Si no se especifica,
     * se utiliza la constante MEDIUM.
     *
     * @param string $locale
     * @param int $intlConstant
     * @return string
     */
    public static function getDateFormat($intlConstant=\IntlDateFormatter::MEDIUM, $locale=null)
    {
        if (is_null($locale)) $locale = \Locale::getDefault();
        $format = \IntlDateFormatter::create($locale, $intlConstant, \IntlDateFormatter::NONE)->getPattern();

        return $format;
    }


    /**
     * Este método convierte recursivamente un array a un objeto (stdclass).
     *
     * @param $a
     * @return object
     */
    public static function arrayToObject($a)
    {
        return is_array($a) ? (object)array_map(__METHOD__, $a) : $a;
    }

}
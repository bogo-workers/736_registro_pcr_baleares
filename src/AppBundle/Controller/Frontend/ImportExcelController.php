<?php

namespace AppBundle\Controller\Frontend;

use AppBundle\Entity\AuxDataType;
use AppBundle\Entity\Registry;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class ImportExcelController extends Controller
{

    /**
     * @Route("/load2015", name="bogo_frontend_default_load2015")
     *
     */
    public function load2015Action()
    {
        $em = $this->getDoctrine()->getEntityManager();

        //Cargo el excel con los datos de 2015 y los dejo en un array
        $path = $this->get('kernel')->getRootDir().'/Resources/datos-excel/2015.xlsx';
        $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject($path);
        $rawData = $phpExcelObject->getActiveSheet()->toArray();    //Array con los datos a cholón
        $fieldNames = $rawData[1];  //Nombres de los campos

        //Indexo todos los datos de todas las filas por los nombres de los campos
        $data = array();
        for ($i=2; $i<=28; $i++) {
            $data[] = array_combine($fieldNames, $rawData[$i]);
        }

        //Obtengo todos los tipos
        $types = $em->getRepository('AppBundle:AuxDataType')->findAll();

        /* ESTO COMPRUEBA LA VALIDEZ DE LOS DATOS DEL EXCEL. UNA VEZ AJUSTADO, SE COMENTA Y SE EJECUTA EL CÓDIGO QUE IMPORTA LOS DATOS */
        //Recorro los datos
        /*
        $i = 3; //Las dos primeras líneas del excel no tienen datos
        foreach ($data as $line) {  //Cada iteración es una línea del excel
            echo '<h3>LINE '.$i.'</h3>';
            foreach ($types as $type) { //Para cada línea, recorro los tipos
                $auxData = $type->getAuxData()->getValues();    //Obtengo los posibles valores asociados a cada tipo
                $found = false;
                foreach ($auxData as $ad) { //Y compruebo si el valor del excel está vacío o es un valor válido
                    $value = trim($line[$type->getShortName()]);
                    if (empty($value) || is_null($value) || strtolower($value) == strtolower($ad->getName())) {
                        //echo $type->getShortName().' => VALID VALUE ('.$line[$type->getShortName()].')<br>';
                        $found = true;
                        break;
                    }
                }
                if (!$found) { //En caso contrario, lo indico
                    echo '<b>'.$type->getShortName().' => INVALID VALUE FOR '.$type->getShortName().' ('.$line[$type->getShortName()].')</b><br>';
                }
            }
            $i++;
            echo '<hr>';
        }
        */

        /*ESTO IMPORTA LOS DATOS*/
        //Recorro los datos
        $i = 3; //Las dos primeras líneas del excel no tienen datos
        foreach ($data as $line) {  //Cada iteración es una línea del excel
            $registry = new Registry();
            $registry->setOschar($line['oschar']==='SI');
            $registry->setNombre($line['nombre']);
            $registry->setFecha(new \DateTime($line['fecha']));
            if (!empty($line['edad'])) $registry->setEdad($line['edad']);
            if (!empty($line['codigoServicio'])) $registry->setCodigoServicio((string)$line['codigoServicio']);
            if (!empty($line['numeroServicio'])) $registry->setNumeroServicio((string)$line['numeroServicio']);

            if (!empty($line['otro'])) $registry->setOtro($line['otro']);
            if (!empty($line['horaLlamada'])) $registry->setHoraLlamada(new \DateTime($line['horaLlamada']));
            if (!empty($line['horaActivacion'])) $registry->setHoraActivacion(new \DateTime($line['horaActivacion']));
            if (!empty($line['horaPcrEstimada'])) {
                $registry->setHoraPcr(new \DateTime($line['horaPcrEstimada']));
                $registry->setEsHoraPcrEstimada(true);
            }
            else {
                $registry->setHoraPcr(new \DateTime($line['horaPcr']));
            }
            if (!empty($line['horaPrimerRcp'])) $registry->setHoraPrimerRcp(new \DateTime($line['horaPrimerRcp']));
            if (!empty($line['duracionRcp'])) $registry->setDuracionRcp((int)$line['duracionRcp']);
            if (!empty($line['hcHospital'])) $registry->setHcHospital((string)$line['hcHospital']);
            if (!empty($line['tiempoRcpa'])) $registry->setTiempoRcpa((int)$line['tiempoRcpa']);
            if (!empty($line['tiempoDemora'])) $registry->setTiempoDemora((int)$line['tiempoDemora']);
            if (!empty($line['noDesfDesa'])) $registry->setNoDesfDesa((int)$line['noDesfDesa']);
            if (!empty($line['noDesfRcpa'])) $registry->setNoDesfRcpa((int)$line['noDesfRcpa']);
            if (!empty($line['fechaAltaHospital'])) $registry->setFechaAltaHospital(new \DateTime($line['fechaAltaHospital']));
            if (!empty($line['fechaEvolucion6meses'])) $registry->setFechaEvolucion6Meses(new \DateTime($line['fechaEvolucion6meses']));
            if (!empty($line['fechaEvolucionAnio'])) $registry->setFechaEvolucionAnio(new \DateTime($line['fechaEvolucionAnio']));

            foreach ($types as $type) { //Para cada línea, recorro los tipos
                $auxData = $type->getAuxData()->filter(
                    function($entry) use ($line, $type) {
                        return $entry->getName() == trim($line[$type->getShortName()]);
                    });
                if (!is_null($auxData) && $auxData->count()==1) {
                    $method = 'set'.strtolower($type->getShortName());
                    $registry->$method($auxData->first());
                }
            }

            $em->persist($registry);
            $i++;
        }
        $em->flush();

        return $this->render(':frontend:index.html.twig');
    }

    /**
     * @Route("/load2014", name="bogo_frontend_default_load2014")
     *
     */
    public function load2014Action()
    {
        $em = $this->getDoctrine()->getEntityManager();

        //Cargo el excel con los datos de 2014 y los dejo en un array
        $path = $this->get('kernel')->getRootDir().'/Resources/datos-excel/2014.xlsx';
        $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject($path);
        $rawData = $phpExcelObject->getActiveSheet()->toArray();    //Array con los datos a cholón
        $fieldNames = $rawData[1];  //Nombres de los campos

        //Indexo todos los datos de todas las filas por los nombres de los campos
        $data = array();
        for ($i=2; $i<=380; $i++) {
            $data[] = array_combine($fieldNames, $rawData[$i]);
        }

        //Obtengo todos los tipos
        $types = $em->getRepository('AppBundle:AuxDataType')->findAll();

        /* ESTO COMPRUEBA LA VALIDEZ DE LOS DATOS DEL EXCEL. UNA VEZ AJUSTADO, SE COMENTA Y SE EJECUTA EL CÓDIGO QUE IMPORTA LOS DATOS */
        //Recorro los datos
        /*
        $i = 3; //Las dos primeras líneas del excel no tienen datos
        foreach ($data as $line) {  //Cada iteración es una línea del excel
            echo '<h3>LINE '.$i.'</h3>';
            foreach ($types as $type) { //Para cada línea, recorro los tipos
                $auxData = $type->getAuxData()->getValues();    //Obtengo los posibles valores asociados a cada tipo
                $found = false;
                foreach ($auxData as $ad) { //Y compruebo si el valor del excel está vacío o es un valor válido
                    $value = trim($line[$type->getShortName()]);
                    if (empty($value) || is_null($value) || strtolower($value) == strtolower($ad->getName())) {
                        //echo $type->getShortName().' => VALID VALUE ('.$line[$type->getShortName()].')<br>';
                        $found = true;
                        break;
                    }
                }
                if (!$found) { //En caso contrario, lo indico
                    echo '<b>'.$type->getShortName().' => INVALID VALUE FOR '.$type->getShortName().' ('.$line[$type->getShortName()].')</b><br>';
                }
            }
            $i++;
            echo '<hr>';
        }
        */

        /*ESTO IMPORTA LOS DATOS*/
        //Recorro los datos
        $i = 3; //Las dos primeras líneas del excel no tienen datos
        foreach ($data as $line) {  //Cada iteración es una línea del excel
            $registry = new Registry();
            $registry->setOschar($line['oschar']==='SI');
            $registry->setNombre($line['nombre']);
            $registry->setFecha(new \DateTime($line['fecha']));
            if (!empty($line['edad'])) $registry->setEdad($line['edad']);
            if (!empty($line['codigoServicio'])) $registry->setCodigoServicio((string)$line['codigoServicio']);
            if (!empty($line['numeroServicio'])) $registry->setNumeroServicio((string)$line['numeroServicio']);

            if (!empty($line['otro'])) $registry->setOtro($line['otro']);
            if (!empty($line['horaLlamada'])) $registry->setHoraLlamada(new \DateTime($line['horaLlamada']));
            if (!empty($line['horaActivacion'])) $registry->setHoraActivacion(new \DateTime($line['horaActivacion']));
            if (!empty($line['horaPcrEstimada'])) {
                $registry->setHoraPcr(new \DateTime($line['horaPcrEstimada']));
                $registry->setEsHoraPcrEstimada(true);
            }
            else {
                $registry->setHoraPcr(new \DateTime($line['horaPcr']));
            }
            if (!empty($line['horaPrimerRcp'])) $registry->setHoraPrimerRcp(new \DateTime($line['horaPrimerRcp']));
            if (!empty($line['duracionRcp'])) $registry->setDuracionRcp((int)$line['duracionRcp']);
            if (!empty($line['hcHospital'])) $registry->setHcHospital((string)$line['hcHospital']);
            if (!empty($line['tiempoRcpa'])) $registry->setTiempoRcpa((int)$line['tiempoRcpa']);
            if (!empty($line['tiempoDemora'])) $registry->setTiempoDemora((int)$line['tiempoDemora']);
            if (!empty($line['noDesfDesa'])) $registry->setNoDesfDesa((int)$line['noDesfDesa']);
            if (!empty($line['noDesfRcpa'])) $registry->setNoDesfRcpa((int)$line['noDesfRcpa']);
            if (!empty($line['fechaAltaHospital'])) $registry->setFechaAltaHospital(new \DateTime($line['fechaAltaHospital']));
            if (!empty($line['fechaEvolucion6meses'])) $registry->setFechaEvolucion6Meses(new \DateTime($line['fechaEvolucion6meses']));
            if (!empty($line['fechaEvolucionAnio'])) $registry->setFechaEvolucionAnio(new \DateTime($line['fechaEvolucionAnio']));

            foreach ($types as $type) { //Para cada línea, recorro los tipos
                $auxData = $type->getAuxData()->filter(
                    function($entry) use ($line, $type) {
                        return $entry->getName() == trim($line[$type->getShortName()]);
                    });
                if (!is_null($auxData) && $auxData->count()==1) {
                    $method = 'set'.strtolower($type->getShortName());
                    $registry->$method($auxData->first());
                }
            }

            $em->persist($registry);
            $i++;
        }
        $em->flush();

        return $this->render(':frontend:index.html.twig');
    }

    /**
     * @Route("/load2013", name="bogo_frontend_default_load2013")
     *
     * - NO TIENE COLUMNA "REGULADOR"
     *
     * - TIENE UNA COLUMNA "DESCONOCIDOS" QUE ESTÁ ENTRE "OTRO" Y "LUGARPCR" QUE NO SE LO QUE ES. LA IGNORO EN LA IMPORTACIÓN
     *
     */
    public function load2013Action()
    {
        $em = $this->getDoctrine()->getEntityManager();

        //Cargo el excel con los datos de 2014 y los dejo en un array
        $path = $this->get('kernel')->getRootDir().'/Resources/datos-excel/2013.xlsx';
        $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject($path);
        $rawData = $phpExcelObject->getActiveSheet()->toArray();    //Array con los datos a cholón
        $fieldNames = $rawData[1];  //Nombres de los campos

        //Indexo todos los datos de todas las filas por los nombres de los campos
        $data = array();
        for ($i=2; $i<=442; $i++) {
            $data[] = array_combine($fieldNames, $rawData[$i]);
        }

        //Obtengo todos los tipos
        $types = $em->getRepository('AppBundle:AuxDataType')->findAll();

        /* ESTO COMPRUEBA LA VALIDEZ DE LOS DATOS DEL EXCEL. UNA VEZ AJUSTADO, SE COMENTA Y SE EJECUTA EL CÓDIGO QUE IMPORTA LOS DATOS */
        //Recorro los datos
        /*
        $i = 3; //Las dos primeras líneas del excel no tienen datos
        foreach ($data as $line) {  //Cada iteración es una línea del excel
            echo '<h3>LINE '.$i.'</h3>';
            foreach ($types as $type) { //Para cada línea, recorro los tipos
                $auxData = $type->getAuxData()->getValues();    //Obtengo los posibles valores asociados a cada tipo
                $found = false;
                foreach ($auxData as $ad) { //Y compruebo si el valor del excel está vacío o es un valor válido
                    $value = trim($line[$type->getShortName()]);
                    if (empty($value) || is_null($value) || strtolower($value) == strtolower($ad->getName())) {
                        //echo $type->getShortName().' => VALID VALUE ('.$line[$type->getShortName()].')<br>';
                        $found = true;
                        break;
                    }
                }
                if (!$found) { //En caso contrario, lo indico
                    echo '<b>'.$type->getShortName().' => INVALID VALUE FOR '.$type->getShortName().' ('.$line[$type->getShortName()].')</b><br>';
                }
            }
            $i++;
            echo '<hr>';
        }
        */

        /*ESTO IMPORTA LOS DATOS*/
        //Recorro los datos
        $i = 3; //Las dos primeras líneas del excel no tienen datos
        foreach ($data as $line) {  //Cada iteración es una línea del excel
            $registry = new Registry();
            $registry->setOschar($line['oschar']==='SI');
            $registry->setNombre($line['nombre']);
            $registry->setFecha(new \DateTime($line['fecha']));
            if (!empty($line['edad'])) $registry->setEdad($line['edad']);
            if (!empty($line['codigoServicio'])) $registry->setCodigoServicio((string)$line['codigoServicio']);
            if (!empty($line['numeroServicio'])) $registry->setNumeroServicio((string)$line['numeroServicio']);

            if (!empty($line['otro'])) $registry->setOtro($line['otro']);
            if (!empty($line['horaLlamada'])) $registry->setHoraLlamada(new \DateTime($line['horaLlamada']));
            if (!empty($line['horaActivacion'])) $registry->setHoraActivacion(new \DateTime($line['horaActivacion']));
            if (!empty($line['horaPcrEstimada'])) {
                $registry->setHoraPcr(new \DateTime($line['horaPcrEstimada']));
                $registry->setEsHoraPcrEstimada(true);
            }
            else {
                $registry->setHoraPcr(new \DateTime($line['horaPcr']));
            }
            if (!empty($line['horaPrimerRcp'])) $registry->setHoraPrimerRcp(new \DateTime($line['horaPrimerRcp']));
            if (!empty($line['duracionRcp'])) $registry->setDuracionRcp((int)$line['duracionRcp']);
            if (!empty($line['hcHospital'])) $registry->setHcHospital((string)$line['hcHospital']);
            if (!empty($line['tiempoRcpa'])) $registry->setTiempoRcpa((int)$line['tiempoRcpa']);
            if (!empty($line['tiempoDemora'])) $registry->setTiempoDemora((int)$line['tiempoDemora']);
            if (!empty($line['noDesfDesa'])) $registry->setNoDesfDesa((int)$line['noDesfDesa']);
            if (!empty($line['noDesfRcpa'])) $registry->setNoDesfRcpa((int)$line['noDesfRcpa']);
            if (!empty($line['fechaAltaHospital'])) $registry->setFechaAltaHospital(new \DateTime($line['fechaAltaHospital']));
            if (!empty($line['fechaEvolucion6meses'])) $registry->setFechaEvolucion6Meses(new \DateTime($line['fechaEvolucion6meses']));
            if (!empty($line['fechaEvolucionAnio'])) $registry->setFechaEvolucionAnio(new \DateTime($line['fechaEvolucionAnio']));

            foreach ($types as $type) { //Para cada línea, recorro los tipos
                if ($type->getShortName() != 'desconocidos') {  //Hay un campo "desconocidos" que no se lo que hacer con él
                    $auxData = $type->getAuxData()->filter(
                        function ($entry) use ($line, $type) {
                            return $entry->getName() == trim($line[$type->getShortName()]);
                        }
                    );
                    if (!is_null($auxData) && $auxData->count() == 1) {
                        $method = 'set'.strtolower($type->getShortName());
                        $registry->$method($auxData->first());
                    }
                }
            }
            $em->persist($registry);
            $i++;
        }
        $em->flush();

        return $this->render(':frontend:index.html.twig');
    }
}

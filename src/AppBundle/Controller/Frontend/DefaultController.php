<?php

namespace AppBundle\Controller\Frontend;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="bogo_frontend_default_index")
     *
     * @param Request $request
     * @return null
     */
    public function indexAction()
    {
        return $this->redirectToRoute('bogo_backnet_default_index');
    }
}

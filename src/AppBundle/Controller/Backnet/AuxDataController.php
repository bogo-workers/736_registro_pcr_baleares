<?php

namespace AppBundle\Controller\Backnet;

use AppBundle\Entity\AuxData;
use AppBundle\Form\AuxDataFormType;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\EntityManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class AuxDataController extends Controller
{
    /**
     * @return null
     */
    public function indexAction()
    {

    }

    /**
     * Listado de un dato auxiliar. Utilizamos la misma acción para listar cualquier
     * tipo de dato auxiliar (puede ser un registro pcr, una unidad, etc).
     *
     * Por eso, recibimos el tipo como agumento en la petición.
     * Hay que comprobar que el tipo sea válido. Podemos hacerlo de dos formas. O bien
     * mediante el método que devuelve los tipos auxiliares que existen directamente de
     * la clase AuxDataTypes (los calcula a partir de las ctes definidas para cada tipo),
     * o bien consultando directamente a la base de datos, mirando los tipos que hay.
     *
     * @Route("/aux/list/{type}", defaults={"type"=null}, name="bogo_backnet_auxdata_list", options={"expose"=true})
     * @Security("has_role('ROLE_ADMIN')")
     * @Template(":backnet/AuxData:list.html.twig")
     *
     * @param string $type
     * @param Request $request
     * @return null
     */
    public function listAction($type, Request $request)
    {
        // entity manager
        $em = $this->getDoctrine()->getManager();
        $auxType = $em->getRepository('AppBundle:AuxDataType')->findOneByShortName($type);
        if (!is_null($auxType)) {
            // si es una petición AJAX
            if ($request->isXmlHttpRequest()) {
                $output = $em->getRepository('AppBundle:AuxData')->findForDataTables(array('type'=>$auxType));
                return new JsonResponse($output);
            } else {
                return array('auxType'=>$auxType);
            }
        }
        else {
            $this->addFlash('ERROR', $this->get('translator')->trans('No se encontró el tipo de dato auxiliar especificado'));
            //Aquí no tenemos tipo para redirigir a su listado, así que redirigimos a ...
            return $this->redirect($this->generateUrl('bogo_backnet_default_index'));
        }
    }


    /**
     * @Route("/aux/add/{type}", name="bogo_backnet_auxdata_add")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template(":backnet/AuxData:handle.html.twig")
     *
     * @param $type
     * @param Request $request
     * @return array
     */
    public function addAction($type, Request $request)
    {
        $auxType = null;
        $form = null;
        $errors = array();
        $translator = $this->get('translator');

        try {
            //Lo primero, comprobamos si existe el tipo de dato auxiliar que hemos recibido en la petición
            $em = $this->getDoctrine()->getManager();
            $auxType = $em->getRepository('AppBundle:AuxDataType')->findOneByShortName($type);
            if (!is_null($auxType)) {
                $entity = new AuxData(); //Nueva entidad
                //Creamos el formulario (añadir)
                $form = $this->createForm(new AuxDataFormType(), $entity);

                //Si hay algún problema al crear el formulario lanzamos una excepción
                if (is_null($form)) {
                    $this->addFlash('ERROR', $translator->trans('Error al intentar crear el formulario'));
                    return $this->redirect($this->generateUrl('bogo_backnet_auxdata_list', array('type' => $type)));
                }

                //Procesamos el form
                $form->handleRequest($request);
                if ($form->isValid()) {   //Si existe y los datos del formulario son correctos
                    $entity->setType($auxType); //El tipo lo ponemos a mano (no lo gestiona el formulario)
                    $em->persist($entity);  //Guardamos los cambios
                    $em->flush();

                    //return $this->redirect($this->generateUrl('bogo_backnet_aux-data_call-reason_show', array('id' => $entity->getId())));
                    return $this->redirect($this->generateUrl('bogo_backnet_auxdata_list', array('type' => $type)));
                }
            }
            else {
                $this->addFlash('ERROR', $translator->trans('No se puede agregar un dato auxiliar del tipo especificado'));
                //Aquí no tenemos tipo para redirigir a su listado, así que redirigimos a ...
                return $this->redirect($this->generateUrl('bogo_backnet_default_index'));
            }
        }
        catch (DBALException $e) {
            $errors['title'] = $translator->trans('Se ha producido un error al intentar añadir un nuevo dato auxiliar');
            $errors['message'] = $translator->trans('El campo "Nombre" no puede estar repetido');
        }
        catch (\Exception $e) {
            $errors['title'] = $translator->trans('Se ha producido un error al intentar añadir un nuevo dato auxiliar');
            $errors['message'] = $e->getMessage();
        }

        //Terminamos y devolvemos a la vista el tipo de dato auxiliar, el formulario y los errores (o no)
        return array(
            'auxType' => $auxType,
            'form'   => $form->createView(),
            'errors' => $errors,
        );
    }

    /**
     * @Route("/aux/edit/{id}", name="bogo_backnet_auxdata_edit", options={"expose"=true})
     * @Security("has_role('ROLE_ADMIN')")
     * @Template(":backnet/AuxData:handle.html.twig")
     *
     * @param $id
     * @param Request $request
     * @return array
     */
    public function editAction($id, Request $request)
    {
        $auxType = null;
        $form = null;
        $errors = array();
        $translator = $this->get('translator');

        try {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:AuxData')->find($id);
            if (!is_null($entity) && $entity->getId()) {
                $auxType = $entity->getType();
                //Creamos el formulario (editar)
                $form = $this->createForm(new AuxDataFormType(), $entity);

                //Si hay algún problema al crear el formulario lanzamos una excepción
                if (is_null($form)) throw new \Exception($translator->trans('Error al intentar crear el formulario'));

                //Procesamos el form
                $form->handleRequest($request);
                if ($form->isValid()) {   //Si existe y los datos del formulario son correctos
                    $em->flush();

                    $this->addFlash('SUCCESS', $translator->trans('Actualización realizada con éxito'));
                    return $this->redirect($this->generateUrl('bogo_backnet_auxdata_edit', array('id' => $id)));
                }
            }
            else {
                $this->addFlash('ERROR', $translator->trans('No se encuentra el dato auxiliar con el identificador especificado'));
                //Aquí no tenemos tipo para redirigir a su listado, así que redirigimos a ...
                return $this->redirect($this->generateUrl('bogo_backnet_default_index'));
            }
        }
        catch (DBALException $e) {
            $errors['title'] = $translator->trans('Se ha producido un error al intentar editar un dato auxiliar');
            $errors['message'] = $translator->trans('El campo "Nombre" no puede estar repetido');
        }
        catch (\Exception $e) {
            //Si salta una excepción y el formulario no se ha creado, hay que volver al listado
            if (is_null($form)) {
                $this->addFlash('ERROR', $e->getMessage());
                return $this->redirect($this->generateUrl('bogo_backnet_auxdata_list', array('type' => $auxType->getShortName())));
            }
            //Sino, guardamos los errores y se mostrarán en la propia página
            $errors['title'] = $translator->trans('Se ha producido un error al intentar editar un dato auxiliar');
            $errors['message'] = $e->getMessage();
        }

        //Terminamos y devolvemos a la vista el tipo de dato auxiliar, el formulario y los errores (o no)
        return array(
            'auxType' => $auxType,
            'form'   => $form->createView(),
            'errors' => $errors,
        );
    }

    /**
     * @Route("/aux/delete/{id}", name="bogo_backnet_auxdata_delete", options={"expose"=true})
     * @Security("has_role('ROLE_ADMIN')")
     * @Template(":backnet/AuxData:handle.html.twig")
     *
     * @param $id
     * @param Request $request
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction($id, Request $request)
    {
        $auxType = null;
        $form = null;
        $errors = array();
        $translator = $this->get('translator');

        try {
            /** @var EntityManager $em */
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:AuxData')->find($id);

            if (!is_null($entity) && $entity->getId()) {
                $auxType = $entity->getType();

                //Obtengo los registros que tienen asociado este dato auxiliar en concreto
                $regs = $entity->getRegistries();
                if ($regs->count() != 0) throw new \Exception($translator->trans('No se puede eliminar el elemento seleccionado. Tiene registros asociados.'));

                //Creamos el formulario (eliminar)
                $form = $this->createForm(new AuxDataFormType(), $entity, array('disabled'=>true));

                //Si hay algún problema al crear el formulario lanzamos una excepción
                if (is_null($form)) throw new \Exception($translator->trans('Error al intentar crear el formulario'));

                //Procesamos el form
                $form->handleRequest($request);
                if ($form->isValid()) {   //Si existe y los datos del formulario son correctos
                    $em->remove($entity);
                    $em->flush();

                    $this->addFlash('SUCCESS', $translator->trans('Eliminación realizada con éxito'));
                    return $this->redirect($this->generateUrl('bogo_backnet_auxdata_list', array('type' => $auxType->getShortName())));
                }
            }
            else {
                $this->addFlash('ERROR', $translator->trans('No se encuentra el dato auxiliar con el identificador especificado'));
                //Aquí no tenemos tipo para redirigir a su listado, así que redirigimos a ...
                return $this->redirect($this->generateUrl('bogo_backnet_default_index'));
            }
        }
        catch (DBALException $e) {
            $errors['title'] = $translator->trans('Se ha producido un error al intentar editar un dato auxiliar');
            $errors['message'] = $e->getMessage();
        }
        catch (\Exception $e) {
            //Si salta una excepción y el formulario no se ha creado, hay que volver al listado
            if (is_null($form)) {
                $this->addFlash('ERROR', $e->getMessage());
                return $this->redirect($this->generateUrl('bogo_backnet_auxdata_list', array('type' => $auxType->getShortName())));
            }
            //Sino, guardamos los errores y se mostrarán en la propia página
            $errors['title'] = $translator->trans('Se ha producido un error al intentar eliminar un dato auxiliar');
            $errors['message'] = $e->getMessage();
        }

        //Terminamos y devolvemos a la vista el tipo de dato auxiliar, el formulario y los errores (o no)
        return array(
            'auxType' => $auxType,
            'form'   => $form->createView(),
            'errors' => $errors,
        );
    }
}

<?php

namespace AppBundle\Controller\Backnet;

use Doctrine\DBAL\DBALException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class UserController extends Controller
{
    /**
     * @Route("/user/list", name="bogo_users_list", options={"expose"=true})
     * @Security("has_role('ROLE_ADMIN')")
     * @Template(":backnet/User:list.html.twig")
     *
     */
    public function listAction(Request $request)
    {
        // si es una petición AJAX
        if ($request->isXmlHttpRequest()) {
            $em = $this->getDoctrine()->getManager();
            $output = $em->getRepository('Bogo:User')->findForDataTables();
            return new JsonResponse($output);
        } else {
            return array();
        }
    }

    /**
     * @Route("/user/add", name="bogo_users_add", options={"expose"=true})
     * @Security("has_role('ROLE_ADMIN')")
     * @Template(":backnet/User:handle.html.twig")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function addAction(Request $request)
    {
        $form = null;
        $errors = array();
        $translator = $this->get('translator');

        try {
            $userManager = $this->get('fos_user.user_manager');
            $user = $userManager->createUser();

            //Creamos el formulario (añadir). Recordar que está como servicio (debido a FOSUserBundle), así que podemos cargarlo directo
            $form = $this->createForm('backnet_user_form', $user);

            //Si hay algún problema al crear el formulario lanzamos una excepción
            if (is_null($form)) {
                $this->addFlash('ERROR', $translator->trans('Error al intentar crear el formulario'));
                return $this->redirect($this->generateUrl('bogo_backnet_registry_list'));
            }

            //Procesamos el form
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {   //Si existe y los datos del formulario son correctos
                $userManager->updateUser($user);

                $this->addFlash('SUCCESS', $translator->trans('Usuario creado con éxito'));
                return $this->redirect($this->generateUrl('bogo_users_edit', array('id' => $user->getId())));
            }
        }
        catch (DBALException $e) {
            $errors['title'] = $translator->trans('Se ha producido un error al intentar añadir un nuevo usuario');
            $errors['message'] = $translator->trans('El campo de email (usuario) no puede estar repetido');
        }
        catch (\Exception $e) {
            $errors['title'] = $translator->trans('Se ha producido un error al intentar añadir un nuevo usuario');
            $errors['message'] = $e->getMessage();
        }

        //Terminamos
        return array(
            'form'   => $form->createView(),
            'errors' => $errors,
        );
    }

    /**
     * @Route("/user/edit/{id}", name="bogo_users_edit", options={"expose"=true})
     * @Security("has_role('ROLE_ADMIN')")
     * @Template(":backnet/User:handle.html.twig")
     *
     * @param $id
     * @param Request $request
     * @return array|RedirectResponse
     */
    public function editAction($id, Request $request)
    {
        $form = null;
        $errors = array();
        $translator = $this->get('translator');

        try {
            $userManager = $this->get('fos_user.user_manager');
            $user = $userManager->findUserBy(array('id'=>$id));

            if (!is_null($user) && $user->getId()) {
                //Creamos el formulario (editar). Recordar que está como servicio (debido a FOSUserBundle), así que podemos cargarlo directo
                $form = $this->createForm('backnet_user_form', $user);

                //Si hay algún problema al crear el formulario lanzamos una excepción
                if (is_null($form)) throw new \Exception($translator->trans('Error al intentar crear el formulario'));

                //Procesamos el form
                $form->handleRequest($request);
                if ($form->isSubmitted() && $form->isValid()) {   //Si existe y los datos del formulario son correctos
                    $userManager->updateUser($user);

                    $this->addFlash('SUCCESS', $translator->trans('Actualización realizada con éxito'));
                    return $this->redirect($this->generateUrl('bogo_users_edit', array('id' => $id)));
                }
            }
            else {
                $this->addFlash('ERROR', $translator->trans('No se encuentra usuario con el identificador especificado'));
                return $this->redirect($this->generateUrl('bogo_users_list'));
            }
        }
        catch (DBALException $e) {
            $errors['title'] = $translator->trans('Se ha producido un error al intentar editar un usuario');
            $errors['message'] = $translator->trans('El campo email (usuario) no puede estar repetido');
        }
        catch (\Exception $e) {
            //Si salta una excepción y el formulario no se ha creado, hay que volver al listado
            if (is_null($form)) {
                $this->addFlash('ERROR', $e->getMessage());
                return $this->redirect($this->generateUrl('bogo_users_list'));
            }
            //Sino, guardamos los errores y se mostrarán en la propia página
            $errors['title'] = $translator->trans('Se ha producido un error al intentar editar un usuario');
            $errors['message'] = $e->getMessage();
        }

        //Terminamos y devolvemos a la vista el tipo de dato auxiliar, el formulario y los errores (o no)
        return array(
            'form'   => $form->createView(),
            'errors' => $errors,
        );
    }

    /**
     * @Route("/user/delete/{id}", name="bogo_users_delete", options={"expose"=true})
     * @Security("has_role('ROLE_ADMIN')")
     * @Template(":backnet/User:handle.html.twig")
     *
     * @param $id
     * @param Request $request
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction($id, Request $request)
    {
        $form = null;
        $errors = array();
        $translator = $this->get('translator');

        if ($this->getUser()->getId() == $id) {
            $this->addFlash('ERROR', $translator->trans('No puede eliminar el usuario con el que ha hecho login actualmente'));
            return $this->redirect($this->generateUrl('bogo_users_list'));
        }

        try {
            $userManager = $this->get('fos_user.user_manager');
            $user = $userManager->findUserBy(array('id'=>$id));

            if (!is_null($user) && $user->getId()) {
                //Creamos el formulario (eliminar). Recordar que está como servicio (debido a FOSUserBundle), así que podemos cargarlo directo
                $form = $this->createForm('backnet_user_form', $user, array('disabled'=>true));

                //Si hay algún problema al crear el formulario lanzamos una excepción
                if (is_null($form)) throw new \Exception($translator->trans('Error al intentar crear el formulario'));

                //Procesamos el form
                $form->handleRequest($request);
                if ($form->isSubmitted() && $form->isValid()) {   //Si existe y los datos del formulario son correctos
                    $userManager->deleteUser($user);

                    $this->addFlash('SUCCESS', $translator->trans('Usuario eliminado con éxito'));
                    return $this->redirect($this->generateUrl('bogo_users_list'));
                }
            }
            else {
                $this->addFlash('ERROR', $translator->trans('No se encuentra usuario con el identificador especificado'));
                return $this->redirect($this->generateUrl('bogo_users_list'));
            }
        }
        catch (DBALException $e) {
            $errors['title'] = $translator->trans('Se ha producido un error al intentar eliminar un usuario');
            $errors['message'] = $e->getMessage();
        }
        catch (\Exception $e) {
            //Si salta una excepción y el formulario no se ha creado, hay que volver al listado
            if (is_null($form)) {
                $this->addFlash('ERROR', $e->getMessage());
                return $this->redirect($this->generateUrl('bogo_users_list'));
            }
            //Sino, guardamos los errores y se mostrarán en la propia página
            $errors['title'] = $translator->trans('Se ha producido un error al intentar eliminar un usuario');
            $errors['message'] = $e->getMessage();
        }

        //Terminamos y devolvemos a la vista el tipo de dato auxiliar, el formulario y los errores (o no)
        return array(
            'form'   => $form->createView(),
            'errors' => $errors,
        );
    }
}

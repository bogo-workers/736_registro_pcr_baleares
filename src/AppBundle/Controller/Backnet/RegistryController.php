<?php

namespace AppBundle\Controller\Backnet;

use AppBundle\Entity\Registry;
use AppBundle\Form\RegistryFormType;
use Doctrine\DBAL\DBALException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;

class RegistryController extends Controller
{
    /**
     * @return null
     */
    public function indexAction()
    {

    }

    /**
     * Listado de registros
     *
     * @Route("/registry/list", name="bogo_backnet_registry_list", options={"expose"=true})
     * @Template(":backnet/Registry:list.html.twig")
     *
     * @param string $type
     * @param Request $request
     * @return null
     */
    public function listAction(Request $request)
    {
        // si es una petición AJAX
        if ($request->isXmlHttpRequest()) {
            $em = $this->getDoctrine()->getManager();
            $output = $em->getRepository('AppBundle:Registry')->findForDataTables();
            return new JsonResponse($output);
        } else {
            return array();
        }
    }


    /**
     * @Route("/registry/add", name="bogo_backnet_registry_add")
     * @Template(":backnet/Registry:handle.html.twig")
     *
     * @param Request $request
     * @return array
     */
    public function addAction(Request $request)
    {
        $form = null;
        $errors = array();
        $translator = $this->get('translator');

        try {
            $em = $this->getDoctrine()->getManager();
            $entity = new Registry(); //Nueva entidad
            //Creamos el formulario (añadir)
            $form = $this->createForm(new RegistryFormType($this->container), $entity);

            //Si hay algún problema al crear el formulario lanzamos una excepción
            if (is_null($form)) {
                $this->addFlash('ERROR', $translator->trans('Error al intentar crear el formulario'));
                return $this->redirect($this->generateUrl('bogo_backnet_registry_list'));
            }

            //Procesamos el form
            $form->handleRequest($request);
            if ($form->isValid()) {   //Si existe y los datos del formulario son correctos
                $em->persist($entity);  //Guardamos los cambios
                $em->flush();

                $this->addFlash('SUCCESS', $translator->trans('Registro creado con éxito'));
                return $this->redirect($this->generateUrl('bogo_backnet_registry_edit', array('id' => $entity->getId())));
            }
        }
        catch (DBALException $e) {
            $errors['title'] = $translator->trans('Se ha producido un error al intentar añadir un nuevo registro');
            $errors['message'] = $translator->trans('El campo "Nombre" no puede estar repetido');
        }
        catch (\Exception $e) {
            $errors['title'] = $translator->trans('Se ha producido un error al intentar añadir un nuevo registro');
            $errors['message'] = $e->getMessage();
        }

        //Terminamos
        return array(
            'form'   => $form->createView(),
            'errors' => $errors,
        );
    }

    /**
     * @Route("/registry/edit/{id}", name="bogo_backnet_registry_edit", options={"expose"=true})
     * @Template(":backnet/Registry:handle.html.twig")
     *
     * @param $id
     * @param Request $request
     * @return array
     */
    public function editAction($id, Request $request)
    {
        $form = null;
        $errors = array();
        $translator = $this->get('translator');
        $response = new Response();

        try {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:Registry')->find($id);
            if (!is_null($entity) && $entity->getId()) {
                //Creamos el formulario (editar)
                $form = $this->createForm(new RegistryFormType($this->container), $entity);

                //Si hay algún problema al crear el formulario lanzamos una excepción
                if (is_null($form)) throw new \Exception($translator->trans('Error al intentar crear el formulario'));

                //Solo procesamos el form si la petición llega por ajax
                if ($request->isXmlHttpRequest()) {
                    $form->handleRequest($request);
                    if ($form->isValid()) {   //Si existe y los datos del formulario son correctos
                        //Guardo cambios
                        $em->flush();

                        $response->setStatusCode(Response::HTTP_OK);
                        $response->setContent($translator->trans('Actualización realizada con éxito'));
                    }
                    else {
                        $response->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR);
                        $response->setContent($translator->trans('Los datos del formulario no son válidos'));
                    }
                    return new JsonResponse($response->getContent(), $response->getStatusCode());
                }
            }
            else {
                $this->addFlash('ERROR', $translator->trans('No se encuentra el registro con el identificador especificado'));
                return $this->redirect($this->generateUrl('bogo_backnet_registry_list'));
            }
        }
        catch (DBALException $e) {
            $msg = $translator->trans('Compruebe los campos que no pueden estar repetidos');
            //Si la excepción se produce durante el procesamiento de una petición ajax, devolvemos con la misma el error
            if ($request->isXmlHttpRequest()) {
                $response->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR);
                $response->setContent($msg);
                return new JsonResponse($response->getContent(), $response->getStatusCode());
            }
            //Sino, estos son los errores que se le pasarán a la vista
            $errors['title'] = $translator->trans('Se ha producido un error al intentar editar un registro');
            $errors['message'] = $msg;
        }
        catch (\Exception $e) {
            //Si salta una excepción y el formulario no se ha creado, hay que volver al listado
            if (is_null($form)) {
                $this->addFlash('ERROR', $e->getMessage());
                return $this->redirect($this->generateUrl('bogo_backnet_registry_list'));
            }

            //Si la excepción se produce durante el procesamiento de una petición ajax, devolvemos con la misma el error
            if ($request->isXmlHttpRequest()) {
                $response->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR);
                $response->setContent($e->getMessage());
                return new JsonResponse($response->getContent(), $response->getStatusCode());
            }
            //Sino, estos son los errores que se le pasarán a la vista
            $errors['title'] = $translator->trans('Se ha producido un error al intentar editar un registro');
            $errors['message'] = $e->getMessage();
        }

        //Terminamos y devolvemos a la vista el formulario y los errores (o no)
        return array(
            'form'   => $form->createView(),
            'errors' => $errors,
        );
    }

    /**
     * @Route("/registry/delete/{id}", name="bogo_backnet_registry_delete", options={"expose"=true})
     * @Template(":backnet/Registry:handle.html.twig")
     *
     * @param $id
     * @param Request $request
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction($id, Request $request)
    {
        $form = null;
        $errors = array();
        $translator = $this->get('translator');

        try {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:Registry')->find($id);

            if (!is_null($entity) && $entity->getId()) {
                //Creamos el formulario (eliminar)
                $form = $this->createForm(new RegistryFormType($this->container), $entity, array('disabled'=>true));

                //Si hay algún problema al crear el formulario lanzamos una excepción
                if (is_null($form)) throw new \Exception($translator->trans('Error al intentar crear el formulario'));

                //Procesamos el form
                $form->handleRequest($request);
                if ($form->isValid()) {   //Si existe y los datos del formulario son correctos
                    $em->remove($entity);
                    $em->flush();

                    $this->addFlash('SUCCESS', $translator->trans('Eliminación realizada con éxito'));
                    return $this->redirect($this->generateUrl('bogo_backnet_registry_list'));
                }
            }
            else {
                $this->addFlash('ERROR', $translator->trans('No se encuentra el registro con el identificador especificado'));
                return $this->redirect($this->generateUrl('bogo_backnet_registry_list'));
            }
        }
        catch (DBALException $e) {
            $errors['title'] = $translator->trans('Se ha producido un error al intentar editar un registro');
            $errors['message'] = $e->getMessage();
        }
        catch (\Exception $e) {
            //Si salta una excepción y el formulario no se ha creado, hay que volver al listado
            if (is_null($form)) {
                $this->addFlash('ERROR', $e->getMessage());
                return $this->redirect($this->generateUrl('bogo_backnet_registry_list'));
            }
            //Sino, guardamos los errores y se mostrarán en la propia página
            $errors['title'] = $translator->trans('Se ha producido un error al intentar eliminar un registro');
            $errors['message'] = $e->getMessage();
        }

        //Terminamos y devolvemos a la vista el formulario y los errores (o no)
        return array(
            'form'   => $form->createView(),
            'errors' => $errors,
        );
    }
}

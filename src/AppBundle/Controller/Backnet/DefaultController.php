<?php

namespace AppBundle\Controller\Backnet;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="bogo_backnet_default_index", options={"expose"=true})
     * NOTA: con expose=true estamos diciendo que queremos poder usar la ruta desde js gracias a FOSJsRoutingBundle
     *
     * @param Request $request
     * @return null
     */
    public function indexAction()
    {
        return $this->render(':backnet/Default:index.html.twig');
    }
}

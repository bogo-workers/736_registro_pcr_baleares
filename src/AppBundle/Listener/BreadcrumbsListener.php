<?php
/**
 * Created by PhpStorm.
 * User: carlos
 * Date: 07/04/15
 * Time: 00:34
 */
namespace AppBundle\Listener;

use AppBundle\Helper\BreadcrumbsHelper;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Routing\Router;
use Symfony\Component\Translation\Translator;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;

use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;


/**
 * BreadcrumbsListener (defined in services.yml as a kernel event listener)
 */
class BreadcrumbsListener
{
    protected $router;
    protected $doctrine;
    protected $translator;
    protected $breadcrumbs;

    /**
     * The constructor initializes breadcrumb & Router var,
     * @param Router      $router
     * @param Translator  $tr
     * @param Breadcrumbs $whiteOctoberBreadcrumbs
     */
    public function __construct(Router $router, EntityManager $doctrine, Translator $tr, Breadcrumbs $whiteOctoberBreadcrumbs)
    {
        $this->router 		= $router;
        $this->doctrine     = $doctrine;
        $this->translator   = $tr;
        $this->breadcrumbs	= $whiteOctoberBreadcrumbs;
    }

    /**
     * For each kernel request executes this function.
     * @param GetResponseEvent $event
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        // prepare vars to obtain the route & path of the request
        $request = $event->getRequest();

        // calls to the helper who controls the breadcrumb
        BreadcrumbsHelper::setBreadcrumb($this->router, $this->doctrine, $this->translator, $this->breadcrumbs, $request);
    }
}
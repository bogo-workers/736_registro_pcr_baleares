<?php
/**
 * Created by PhpStorm.
 * User: carlos
 * Date: 07/04/15
 * Time: 00:34
 */
namespace AppBundle\Listener;

use AppBundle\Entity\AuxDataType;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;


/**
 * (defined in services.yml as a kernel event listener)
 */
class SidebarMenuListener
{
    protected $twig;
    protected $doctrine;

    /**
     * Necesito recibir twig porque voy a guardar los bloques como una variable global de twig
     *
     * @param \Twig_Environment $twig
     * @param EntityManager $doctrine
     */
    public function __construct(\Twig_Environment $twig, EntityManager $doctrine)
    {
        $this->twig = $twig;
        $this->doctrine = $doctrine;
    }

    /**
     * Obtenemos los tipos auxiliares clasificados por bloques para poder construir el menú más fácilmente
     * Pasamos de esto:
        array:6 [▼
            0 => array:2 [▼
                "description" => "Datos bloque 1"
                "types" => array:9 [▼
                    0 => "registropcr"
                    1 => "completo"
                    2 => "motivollamada"
                    3 => "unidad"
                    4 => "medico"
                    5 => "due"
                    6 => "regulador"
                    7 => "poblacion"
                    8 => "sexo"
                ]
            ]
            1 => array:2 [▶]
            2 => array:2 [▶]
            3 => array:2 [▶]
            4 => array:2 [▶]
            5 => array:2 [▶]
            ]

     * A esto:
        array:6 [▼
            0 => array:2 [▼
                "description" => "Datos bloque 1"
                "types" => array:9 [▼
                    "registropcr" => AuxDataType {#789 ▼
                        -id: "265"
                        -auxData: PersistentCollection {#381 ▶}
                        -shortName: "registropcr"
                        -description: "Registro PCR"
                    }
                    "completo" => AuxDataType {#379 ▶}
                    "motivollamada" => AuxDataType {#375 ▶}
                    "unidad" => AuxDataType {#373 ▶}
                    "medico" => AuxDataType {#370 ▶}
                    "due" => AuxDataType {#367 ▶}
                    "regulador" => AuxDataType {#363 ▶}
                    "poblacion" => AuxDataType {#362 ▶}
                    "sexo" => AuxDataType {#358 ▶}
                ]
            ]
            1 => array:2 [▶]
            2 => array:2 [▶]
            3 => array:2 [▶]
            4 => array:2 [▶]
            5 => array:2 [▶]
        ]
     *
     * @param GetResponseEvent $event
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        $auxDataTypes = $this->doctrine->getRepository('AppBundle:AuxDataType')->findAll(); //Todos los tipos auxiliares
        $auxDataTypes = array_combine(array_map(function($e){return $e->getShortName();},$auxDataTypes), $auxDataTypes);    //Al array anterior, le ponemos de claves los shortnames de los tipos

        $auxTypesBlocks = AuxDataType::getBlocks(); //Todos los bloques de tipos (cada bloque es un array con su descripción y un array con los nombres cortos de los tipos)
        //Teniendo los tipos por un lado y los bloques con los tipos colgando por otro, mapeamos y dejamos los tipos como entidades en lugar de como nombres cortos
        array_walk($auxTypesBlocks,
            function(&$e) use ($auxDataTypes) {
                $e['types'] = array_flip($e['types']);
                foreach ($e['types'] as $typeShortName=>$v) {
                    $e['types'][$typeShortName] = $auxDataTypes[$typeShortName];
                }
            }
        );

        $this->twig->addGlobal('auxTypesBlocks', $auxTypesBlocks);
    }
}
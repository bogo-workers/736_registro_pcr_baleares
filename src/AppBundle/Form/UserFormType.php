<?php
/**
 * Created by PhpStorm.
 * User: carlos
 * Date: 24/04/15
 * Time: 12:20
 */

namespace AppBundle\Form;

use AppBundle\Form\BacknetBaseFormType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;


class UserFormType extends BacknetBaseFormType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        //Roles existentes. Es un array con los roles como claves y si hay jerarquía los va metiendo en subarrays
        $roles = $this->getServiceContainer()->getParameter('security.role_hierarchy.roles');

        //Campos del formulario
        //IMPORTANTE. El campo de contraseña es un campo NO asociado a la entidad. Es decir, no es el campo
        //que se mapea con el campo password de la entidad. Esto nos facilita el trabajo de manejar el cambio
        //de contraseña (ver el addEventListener para el POST_SUBMIT)
        //IMPORTANTE. Idem para los roles. Los gestionamos por separado. O no he visto como se hace, o symfony
        //se hace un lío al detectar el tipo como una cadena (es el tipo de los roles) aunque esté marcado como
        //array de doctrine. El caso es que cargo yo los roles definidos y los uso.
        $builder
            ->add('email')
            ->add('unmappedPassword', 'password', array(
                'mapped' => false,  //No lo ligamos a la entidad
                //'required' => $this->getFormType() == BacknetBaseFormType::FORM_TYPE_ADD,   //Solo es obligatorio al añadir
                'required' => true,
            ))
            ->add('enabled', null, array('required' => false))
            ->add('unmappedRoles', 'choice', array(
                'mapped' => false,  //No lo ligamos a la entidad
                'choices' => array_combine(array_keys($roles), array_keys($roles)), //Los roles existentes, tanto de claves como valores
            ))

            //Subformularios con los campos de entidades asociadas al usuario
            ->add('profile', new UserProfileFormType())   //Form para entidad UserProfile
            ->add('contactInfo', new UserContactInfoFormType())   //Form para entidad UserContact
            ->add('social', new UserSocialFormType())   //Form para entidad UserSocial
        ;


        //Cuando cargamos los datos
        $builder->addEventListener(FormEvents::POST_SET_DATA, function(FormEvent $event) {
            $entity = $event->getData();    //Entidad
            $form = $event->getForm();  //Formulario
            if ($entity && null !== $entity->getId()) { //Esto solo se hace cuando ya tenemos entidad (no se ejecuta al mostrar el form para añadir por ejemplo)

                //Si el usuario que se está editando es el mismo que hay hecho login, no permito cambiar el rol ni activar/desactivar
                if ($this->getServiceContainer()->get('security.token_storage')->getToken()->getUser()->getId() == $entity->getId()) {
                    $this->changeElementProperty($form->get('unmappedRoles'), array('disabled' => true));
                    $this->changeElementProperty($form->get('enabled'), array('disabled' => true));
                }

                //Tenemos que seleccionar el rol del usuario. Está muy simplificado, manejamos asignación directa de roles, un usuario no tiene varios
                $form->get('unmappedRoles')->setData($entity->getRoles()[0]);

                //Si estamos editando, el campo password NO es obligatorio. Si se deja en blanco se mantiene la contraseña
                $this->changeElementProperty($form->get('unmappedPassword'), array('required' => false));
            }
        });

        //Cuando nos llegan los datos al hacer el submit.
        //Gestionamos el campo de usuario y el de contraseña
        $builder->addEventListener(FormEvents::POST_SUBMIT, function(FormEvent $event) {
            $entity = $event->getData();
            $form = $event->getForm();

            //El username es obligatorio. Nosotros no diferenciamos entre el email y el username, así que vamos a
            //guardar lo mismo en ambos campos
            $entity->setUsername($form->get('email')->getData());

            //El rol seleccionado es el que le asigno al usuario
            $entity->setRoles(array($form->get('unmappedRoles')->getData()));

            //La contraseña va a un campo cifrado. De eso se encarga un método de User al que le podemos pasar la
            //contraseña en claro y él la cifra y la guarda. Solamente procesamos el campo si tiene valor.
            //Si estamos añadiendo un usuario entonces seguro que tenemos valor, ya que el campo es obligatorio. Sino,
            //(en la edición por ej), puede venir con valor, en cuyo caso se actualiza o vacía, en cuyo caso se deja
            //como está
            $password = $form->get('unmappedPassword')->getData();
            if (!empty($password)) {
                $entity->setPlainPassword($password);
            }
        });

        parent::buildForm($builder, $options);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return parent::getName() . 'user_form';
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    //public function setDefaultOptions(OptionsResolver $resolver)  <-- SYMFONY3
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Bogo\Entity\User',
        ));
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: carlos
 * Date: 26/03/15
 * Time: 09:36
 */

namespace AppBundle\Form;

use AppBundle\Form\BacknetBaseFormType;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class RegistryFormType extends BacknetBaseFormType
{

    /**
     * La construcción de este formulario es un poco particular. Para las propiedades "normales" de la entidad no hay
     * nada especial. Se añaden y listo.
     *
     * La parte diferente es la forma de añadir todos los campos correspondientes a los datos auxliares. Si dejáramos
     * el comportamiento por defecto de symfony-doctrine, para una propiedad de tipo relación ManyToMany como es auxData,
     * el formulario presentaría un único campo donde podríamos añadir varios valores de entre toda la lista de datos
     * auxiliares.
     *
     * Dado que este comportamiento no sirve, tenemos que generar un campo para cada tipo de dato auxiliar, y además que
     * al desplegarse solamente muestre los elementos de su tipo.
     *
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        //Añado los propios del registro (los que no son datos auxiliares)
        //No entiendo por qué tengo que poner a no required algunos campos (los de horas y fechas) cuando
        //ya están como nullables en la definición de la entidad
        $builder
            ->add('nombre')
            ->add('fecha', 'bogo_date')
            ->add('edad')
            ->add('codigoServicio')
            ->add('numeroServicio')
            ->add('oschar');

        //Obtengo los tipos de datos auxiliares que tengo
        $auxDataTypes = $this->getEntityManager()->getRepository('AppBundle:AuxDataType')->findAll();

        //En el preset_data, si tengo una entidad formada es que la estoy editando (o eliminando), y sino es que
        //voy a añadir una. Solo pongo todos los campos de los datos auxiliares si estoy editando. Para añadir
        //solo muestro los principales y botón de continuar que me lleva a la edición
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function(FormEvent $event) use ($auxDataTypes) {
            $form = $event->getForm();
            //Si el formulario es de edición o eliminación, añadimos los campos para los datos auxiliares
            if (!is_null($event->getData()->getId())) {
                //Añado todos los campos que representan datos auxiliares
                foreach ($auxDataTypes as $auxType) {   //Los recorro
                    $typeName = $auxType->getShortName();   //Este es el nombre que usaré para el nombre de los campos del form
                    $form->add(
                        $typeName,
                        'entity',
                        array(   //Añado un campo con dicho nombre y tipo entidad
                            'class' => 'AppBundle:AuxData', //Su entidad asociada es AuxData
                            'query_builder' => function (EntityRepository $er) use ($typeName) {
                                return $er->filterForFormBuilder(
                                    $typeName
                                );    //Y filtro los datos asociados al campo a solo los de su tipo (ver repositorio de AuxData)
                            },
                            'required' => false,
                            'placeholder' => '' //Necesario para que funcione el placeholder de select2
                        )
                    );
                }
                //Y el resto de campos asociados a los auxiliares
                $form
                    ->add('otro')
                    ->add('horaLlamada', 'bogo_time', array('required' => false))
                    ->add('horaActivacion', 'bogo_time', array('required' => false))
                    ->add('horaPcr', 'bogo_time', array('required' => false))
                    ->add('esHoraPcrEstimada')
                    ->add('horaPrimerRcp', 'bogo_time', array('required' => false))
                    ->add('tiempoDemora')
                    ->add('noDesfDesa')
                    ->add('tiempoRcpa')
                    ->add('noDesfRcpa')
                    ->add('duracionRcp')
                    ->add('otros')
                    ->add('hcHospital')
                    ->add('fechaAltaHospital', 'bogo_date', array('required' => false))
                    ->add('fechaEvolucion6Meses', 'bogo_date', array('required' => false))
                    ->add('fechaEvolucionAnio', 'bogo_date', array('required' => false));
            }
        });

        //Listener para completar los campos de los datos auxiliares en la carga del formulario
        //Cuando ya tenemos los datos en la entidad, recorremos los campos auxiliares y vamos
        //poniendo los valores correspondientes de cada dato auxiliar de la colección
        $builder->addEventListener(FormEvents::POST_SET_DATA, function(FormEvent $event) {
            $entity = $event->getData();    //Entidad
            $form = $event->getForm();  //Formulario
            if ($entity && null !== $entity->getId()) { //Esto solo se hace cuando ya tenemos entidad (no se ejecuta al mostrar el form para añadir por ejemplo)
                foreach ($entity->getAuxData() as $auxData) {   //Recorro los datos auxliares de la entidad
                    $form->get($auxData->getType()->getShortName())->setData($auxData); //Al campo del form con el nombre del tipo, le asigno el dato
                }
            }
        });

        //Listener para el proceso al anterior. Si con el anterior cargamos los valores de la entidad
        //en los campos del formulario que no están directamente mapeados, aquí pasamos a la entidad los
        //valores que nos llegan del formulario
        $builder->addEventListener(FormEvents::POST_SUBMIT, function(FormEvent $event) use ($auxDataTypes) {
            $entity = $event->getData();    //Entidad
            $form = $event->getForm();  //Formulario
            if ($entity && null !== $entity->getId() && !is_null($auxDataTypes)) { //Esto solo se hace cuando ya tenemos entidad (no se ejecuta al mostrar el form para añadir por ejemplo)
                foreach ($auxDataTypes as $type) {  //Recorremos todos los tipos auxiliares que tenemos
                    $method = 'set'.$type->getShortName();  //Construimos el setter que pasa el valor a la entidad
                    $entity->$method($form->get($type->getShortName())->getData()); //Y leemos usando el  tipo el valor del campo del form
                }
            }
        });

        parent::buildForm($builder, $options);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return parent::getName() . 'registry_form';
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    //public function setDefaultOptions(OptionsResolver $resolver)  <-- SYMFONY3
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Registry',
        ));
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: carlos
 * Date: 26/03/15
 * Time: 09:18
 */

namespace AppBundle\Form;

use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;

/**
 * Esta clase sirve de base para los formularios que normalmente se utilizan en Backnet.
 * Estos formularios suelen tener en común algunas cosas que se representan en esta clase
 * (un campo id oculto, se deshabilitan cuando se borran elementos, etc)
 * y así pasan a todos los que la utilicen. Obviamente su uso es opcional.
 *
 * Class BacknetBaseFormType
 * @package AppBundle\Form
 */
abstract class BacknetBaseFormType extends AbstractType
{
    private $container;  //Service container. Disponible para obtener servicios varios

    /**
     * Dejamos como opcional recibir el service container. Cómodo de pasar tanto si el
     * formulario se define como servicio como si se usa con normalidad y se instancia
     *
     * @param string $formType
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * Método que genera el formulario.
     * Especificamos el método (POST) y el campo id que
     * normalmente tenemos en todos los formularios de Backnet.
     *
     * NOTA: Inicialmente yo aquí añadía el botón de submit para enviar el formulario.
     * Según las recomendacioens de Symfony lo he quitado y los botones de los forms
     * se pintan en las plantillas. Mucho más flexible.
     *
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->setMethod("POST")     //El form va por post
            ->add('id', 'hidden');   //Elemento id
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'backnet_';
    }

    /**
     * @return ContainerInterface
     */
    protected function getServiceContainer()
    {
        return $this->container;
    }

    /**
     * @return EntityManager
     */
    protected function getEntityManager()
    {
        return is_null($this->getServiceContainer()) ? null : $this->getServiceContainer()->get('doctrine.orm.entity_manager');
    }

    /**
     * Este método cambiar una propiedad de un elemento de un formulario
     *
     * No he encontrado la forma de recorrer los elementos del formulario y modificar sus propiedades
     * al vuelo (no se si se puede hacer). La única forma que he encontrado que soporte symfony es
     * que al añadir al form un elemento con el mismo nombre, pues lo sobreescribe.
     *
     * Así que recorro los elementos, obtengo su nombre, tipo y opciones y los vuelvo a añadir
     * activando la opción de disabled. No se si es lo más correcto pero funciona OK.
     *
     * @param FormInterface $element
     * @param array $propertyAndValue
     */
    protected function changeElementProperty(FormInterface $element, array $propertyAndValue)
    {
        $elementType = $element->getConfig()->getType()->getInnerType(); //Tipo
        $options = $element->getConfig()->getOptions(); //Opciones actuales del elemento
        $options[key($propertyAndValue)] = current($propertyAndValue);    //Añadimos la de deshabilitar
        $parentForm = $element->getParent();
        $parentForm->remove($element->getName());
        $parentForm->add($element->getName(), $elementType, $options); //Sobreescribimos el elemento
    }

}

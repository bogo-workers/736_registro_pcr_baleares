<?php
/**
 * Created by PhpStorm.
 * User: carlos
 * Date: 04/04/15
 * Time: 18:47
 */

namespace AppBundle\Form\Type;

use AppBundle\Util\BaseUtil;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class BogoDateType
 *
 * Clase para definir un nuevo campo para los tipos fecha en los formularios.
 * Heredamos del campo tipo fecha de symfony, y lo que hacemos es darle un nuevo nombre (bogo_date),
 * forzar a que el widget sea de tipo single_text (para que no salga la colección de combos)
 * y adaptamos el formato al locale activo en el momento.
 *
 * Esto último es lo más importante. Comprobamos el locale que tenemos y obtenemos el formato (patrón)
 * correspondiente a dicho locale. Una vez obtenido es el que le enchufamos al campo.
 *
 * Lo damos de alta como servicio (ver services.yml) para que instanciarlo sea directo y transparente,
 * tal y como se añade cualquier otro campo a un formulario en symfony.
 *
 * @package AppBundle\Form\Type
 */
class BogoDateType extends AbstractType
{

    /**
     * @param OptionsResolverInterface $resolver
     */
    //public function setDefaultOptions(OptionsResolver $resolver)  <-- SYMFONY3
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        //En las opciones, lo aplicamos
        $resolver->setDefaults(array(
            'widget' => 'single_text',
            'format' => BaseUtil::getDateFormat()
        ));
    }

    /**
     * El funcionamiento, tal cual el campo fecha de symfony
     *
     * @return string
     */
    public function getParent()
    {
        return 'date';
    }

    /**
     * Nombre del nuevo tipo. OJO!!!, IMPORTANTE, este nombre ha de coincidir con el que
     * le asignemos al servicio que representa el componente en services.yml
     *
     * @return string
     */
    public function getName()
    {
        return 'bogo_date';
    }
}
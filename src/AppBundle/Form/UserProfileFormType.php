<?php
/**
 * Created by PhpStorm.
 * User: carlos
 * Date: 27/04/15
 * Time: 12:40
 */

namespace AppBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UserProfileFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('document')
            ->add('name')
            ->add('surname1')
            ->add('surname2');
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'backnet_userprofile_form';
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    //public function setDefaultOptions(OptionsResolver $resolver)  <-- SYMFONY3
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Bogo\Entity\UserProfile',
        ));
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: carlos
 * Date: 27/04/15
 * Time: 12:40
 */

namespace AppBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UserSocialFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('facebookUid')
            ->add('facebookName')
            ->add('twitterUid')
            ->add('twitterName');
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'backnet_usersocial_form';
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    //public function setDefaultOptions(OptionsResolver $resolver)  <-- SYMFONY3
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Bogo\Entity\UserSocial',
        ));
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: carlos
 * Date: 26/03/15
 * Time: 09:36
 */

namespace AppBundle\Form;

use AppBundle\Form\BacknetBaseFormType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AuxDataFormType extends BacknetBaseFormType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('state');

        parent::buildForm($builder, $options);
    }

    public function getName()
    {
        return parent::getName() . 'auxdata_form';
    }

    //public function setDefaultOptions(OptionsResolver $resolver)  <-- SYMFONY3
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\AuxData',
        ));
    }
}
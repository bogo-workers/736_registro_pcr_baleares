<?php
/**
 * Created by PhpStorm.
 * User: carlos
 * Date: 22/03/15
 * Time: 10:45
 */

namespace AppBundle\Entity\Repository;

use AppBundle\Entity\AuxData;
use Bogo\Entity\Repository\BaseRepository;


/**
 * AuxDataTypeRepository
 */
class AuxDataRepository extends BaseRepository
{

    /**
     * Este método filtra los elementos que se han de mostrar en los desplegables de los
     * formularios que muestran los tipos auxiliares. Dado que todos los tipos auxiliares
     * se representan con la misma entidad (y se guardan en la misma tabla), cuando en
     * un formulario se incrusta un elemento de un tipo de dato auxiliar determinado, solo
     * han de aparecer los elementos de ese tipo.
     *
     * Para conseguir esto, le pasamos al elemento del formulario la queryBuilder que filtra
     * los elementos. El filtro es el tipo (se recibe como argumento) y que estén activos.
     *
     * @param string $type
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function filterForFormBuilder($type)
    {
        $qb = $this->getEntityManager()->createQueryBuilder('aux');
        $qb ->select('aux')
            ->from('AppBundle:AuxData', 'aux')
            ->leftJoin('aux.type', 'auxtype')
            ->where(
                $qb->expr()->andX(
                    $qb->expr()->eq('auxtype.shortName', ':type'),
                    $qb->expr()->eq('aux.state', ':state')
                )
            )
            ->setParameter('type', $type)
            ->setParameter('state', AuxData::STATE_ENABLED);

        return $qb;
    }
}
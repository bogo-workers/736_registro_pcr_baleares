<?php
/**
 * Created by PhpStorm.
 * User: carlos
 * Date: 02/04/15
 * Time: 09:26
 */

namespace AppBundle\Entity;

use AppBundle\Util\BaseUtil;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\RegistryRepository")
 * @ORM\Table(name="registries")
 */
class Registry
{
    /**
     * @var int|null
     *
     * @ORM\Id
     * @ORM\Column(type="bigint", options={"unsigned":true})
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * Colección de datos auxiliares asociados a un registro
     *
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="AuxData", inversedBy="registries")
     */
    private $auxData;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $nombre;

    /**
     * @var date
     * @ORM\Column(type="date")
     */
    private $fecha;

    /**
     * @var integer
     * @ORM\Column(type="integer", options={"unsigned":true}, nullable=true)
     */
    private $edad;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $codigoServicio;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $numeroServicio;

    /**
     * @var
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $oschar;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $otro;

    /**
     * @var string
     * @ORM\Column(type="time", nullable=true)
     */
    private $horaLlamada;

    /**
     * @var string
     * @ORM\Column(type="time", nullable=true)
     */
    private $horaActivacion;

    /**
     * @var string
     * @ORM\Column(type="time", nullable=true)
     */
    private $horaPcr;

    /**
     * @var string
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $esHoraPcrEstimada;

    /**
     * @var string
     * @ORM\Column(type="time", nullable=true)
     */
    private $horaPrimerRcp;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $tiempoDemora;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $noDesfDesa;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $tiempoRcpa;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $noDesfRcpa;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $duracionRcp;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $otros;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $hcHospital;

    /**
     * @var string
     * @ORM\Column(type="date", nullable=true)
     */
    private $fechaAltaHospital;

    /**
     * @var string
     * @ORM\Column(type="date", nullable=true)
     */
    private $fechaEvolucion6Meses;

    /**
     * @var string
     * @ORM\Column(type="date", nullable=true)
     */
    private $fechaEvolucionAnio;


    /**
     * Los datos auxiliares son una colección. Doctrine exige que se lo digamos en el constructor
     *
     */
    public function __construct()
    {
        $this->auxData = new ArrayCollection();
    }

    /*
     * El formulario que vamos a generar para esta entidad contendrá campos que no están definidos
     * como propiedades de la entidad. Esto hace que el hydrator de doctrine llame a los getters y
     * setters con los nombres de los campos (por ej, getMotivollamada o setUnidad).
     * Dado que no tenemos dichas propiedades ni (lógicamente) sus getters/setters correspondientes,
     * declaramos los métodos mágicos que reciben dichas llamadas.
     * Funcionan simplemente como proxies hacia __call (ver siguiente método) que será el que
     * gestione las llamadas a métodos que no están definidos.
     */
    public function __get($property) {}
    public function __set($property, $value) {}

    /**
     * Método mágico para gestionar los datos auxiliares.
     *
     * La gestión de los datos auxiliares en esta entidad es muy particular. Aunque se gestionan como
     * una colección, hay que tener en cuenta que no se pueden tener dos datos auxiliares del mismo tipo
     * y que tendremos un campo de formulario para cada uno de esos tipos.
     * Esto hace que cada dato auxiliar asociado se trate como si fuera una propiedad de la entidad, aunque
     * en realidad sea un elemento de la colección de datos auxiliares.
     *
     * Este método maneja las llamadas a los getters y setters "virtuales", de tal forma que si yo hago
     * un setUnidad(datoAuxiliar), pues se encarga de introducir (o sustituir) en la lista de datos auxiliares
     * el dato auxiliar de tipo unidad.
     *
     * @param string $name
     * @param array $arguments
     * @return \Doctrine\Common\Collections\Collection
     */
    public function __call($name, array $arguments)
    {
        //Dependiendo del inicio del nombre del método, será un getXXX o setXXX
        $getterOrSetter = substr($name, 0, 3);
        //El resto del nombre del método es el tipo de dato auxiliar (en setUnidad, pues Unidad es el tipo"
        $typeName = strtolower(substr($name, 3));

        //Si es un getter, devolvemos el elemento de la lista de datos auxliares del tipo especificado
        //(ver getAuxData, que está modificado para que funcione así)
        if ($getterOrSetter == 'get') {
            return $this->getAuxData($typeName);
        }
        //Si es un setter, comprobamos que lo que recibimos es correcto y actualizamos la lista de datos auxiliares
        elseif ($getterOrSetter == 'set') {
            //Nos tiene que llegar un único valor, de tipo AuxData y cuyo tipo coincida con el tipo obtenido a partir del nombre del método
            if (count($arguments) == 1 && $arguments[0] instanceof AuxData && $arguments[0]->getType()->getShortName() == $typeName) {
                $existing = $this->getAuxData($typeName);
                if ($existing instanceof AuxData) $this->removeAuxDatum($existing);    //Si hay elemento de ese tipo se borra
                $this->addAuxDatum($arguments[0]);  //Se introduce el nuevo elemento
            }
        }
    }


    /**
     * Add auxData
     * Esta propiedad no se gestiona directamente desde el exterior (ver __call), así que esto es privado.
     *
     * @param \AppBundle\Entity\AuxData $auxData
     * @return Registry
     */
    private function addAuxDatum(AuxData $auxData)
    {
        $this->auxData[] = $auxData;

        return $this;
    }

    /**
     * Remove auxData
     * Esta propiedad no se gestiona directamente desde el exterior (ver __call), así que esto es privado.
     *
     * @param \AppBundle\Entity\AuxData $auxData
     */
    private function removeAuxDatum(AuxData $auxData)
    {
        $this->auxData->removeElement($auxData);
    }

    /**
     * Get auxData
     *
     * Cuando se solicita la colección de datos auxiliares, se puede hacer de dos formas, especificando el
     * tipo o sin hacerlo.
     *
     * Si no se especifica el tipo, se devuelve toda la colección, tal cual. Esto proporciona todos los datos
     * auxiliares asociados al registro (de todos los tipos).
     *
     * Si se especifica el tipo, entonces devolvemos el dato auxiliar de dicho tipo, que solamente puede ser
     * uno o ninguno (solo se puede tener un dato auxiliar de un determinado tipo).
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAuxData($type=null)
    {
        if (!is_null($type)) {
            //Escribimos un closure para pasarle como filtro a la colección
            $hasAuxType = function($typeName) {    //La variable función recibe el tipo
                return function(AuxData $data) use ($typeName) {    //El closure recibe cada objeto de la colección y usa el tipo
                    return $typeName == $data->getType()->getShortName();   //Comprobamos en cada elemento su tipo (mediante el nombre corto y único)
                };
            };
            $element = $this->auxData->filter($hasAuxType($type));  //Aplicamos el filtro
            if ($element->count() == 1) return $element->first();   //Si el encontramos resultado (y solo uno), lo devolvemos
            return new ArrayCollection();   //Sino, colección vacía
        }
        return $this->auxData;  //Si no hay tipo especificado, devolvemos toda la colección
    }


    /* A PARTIR DE AQUÍ GESTIONAMOS LAS PROPIEDADES "NORMALES", LAS QUE NO SON DATOS AUXILIARES */

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $nombre
     * @return Registry
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set date
     *
     * @param \DateTime $fecha
     * @return Registry
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set age
     *
     * @param integer $edad
     * @return Registry
     */
    public function setEdad($edad)
    {
        $this->edad = $edad;

        return $this;
    }

    /**
     * Get age
     *
     * @return integer 
     */
    public function getEdad()
    {
        return $this->edad;
    }

    /**
     * Set serviceCode
     *
     * @param string $codigoServicio
     * @return Registry
     */
    public function setCodigoServicio($codigoServicio)
    {
        $this->codigoServicio = $codigoServicio;

        return $this;
    }

    /**
     * Get serviceCode
     *
     * @return string 
     */
    public function getCodigoServicio()
    {
        return $this->codigoServicio;
    }

    /**
     * Set serviceNumber
     *
     * @param string $numeroServicio
     * @return Registry
     */
    public function setNumeroServicio($numeroServicio)
    {
        $this->numeroServicio = $numeroServicio;

        return $this;
    }

    /**
     * Get serviceNumber
     *
     * @return string 
     */
    public function getNumeroServicio()
    {
        return $this->numeroServicio;
    }

    /**
     * Set oschar
     *
     * @param boolean $oschar
     * @return Registry
     */
    public function setOschar($oschar)
    {
        $this->oschar = $oschar;

        return $this;
    }

    /**
     * Get oschar
     *
     * @return boolean 
     */
    public function getOschar()
    {
        return $this->oschar;
    }
    public function isInOschar()
    {
        return $this->getOschar();
    }

    /**
     * Set otro
     *
     * @param string $otro
     * @return Registry
     */
    public function setOtro($otro)
    {
        $this->otro = $otro;

        return $this;
    }

    /**
     * Get otro
     *
     * @return string
     */
    public function getOtro()
    {
        return $this->otro;
    }

    /**
     * Set horaLlamada
     *
     * @param \DateTime $horaLlamada
     * @return Registry
     */
    public function setHoraLlamada($horaLlamada)
    {
        $this->horaLlamada = $horaLlamada;

        return $this;
    }

    /**
     * Get horaLlamada
     *
     * @return \DateTime
     */
    public function getHoraLlamada()
    {
        return $this->horaLlamada;
    }

    /**
     * Set horaActivacion
     *
     * @param \DateTime $horaActivacion
     * @return Registry
     */
    public function setHoraActivacion($horaActivacion)
    {
        $this->horaActivacion = $horaActivacion;

        return $this;
    }

    /**
     * Get horaActivacion
     *
     * @return \DateTime
     */
    public function getHoraActivacion()
    {
        return $this->horaActivacion;
    }

    /**
     * Set horaPcr
     *
     * @param \DateTime $horaPcr
     * @return Registry
     */
    public function setHoraPcr($horaPcr)
    {
        $this->horaPcr = $horaPcr;

        return $this;
    }

    /**
     * Get horaPcr
     *
     * @return \DateTime
     */
    public function getHoraPcr()
    {
        return $this->horaPcr;
    }

    /**
     * Set esHoraPcrEstimada
     *
     * @param boolean $esEstimada
     * @return Registry
     */
    public function setEsHoraPcrEstimada($esEstimada)
    {
        $this->esHoraPcrEstimada = $esEstimada;

        return $this;
    }

    /**
     * Get esHoraPcrEstimada
     *
     * @return boolean
     */
    public function getEsHoraPcrEstimada()
    {
        return $this->esHoraPcrEstimada;
    }

    /**
     * Set horaPrimerRcp
     *
     * @param \DateTime $horaPrimerRcp
     * @return Registry
     */
    public function setHoraPrimerRcp($horaPrimerRcp)
    {
        $this->horaPrimerRcp = $horaPrimerRcp;

        return $this;
    }

    /**
     * Get horaPrimerRcp
     *
     * @return \DateTime
     */
    public function getHoraPrimerRcp()
    {
        return $this->horaPrimerRcp;
    }

    /**
     * Set tiempoDemora
     *
     * @param string $tiempoDemora
     * @return Registry
     */
    public function setTiempoDemora($tiempoDemora)
    {
        $this->tiempoDemora = $tiempoDemora;

        return $this;
    }

    /**
     * Get tiempoDemora
     *
     * @return string
     */
    public function getTiempoDemora()
    {
        return $this->tiempoDemora;
    }

    /**
     * Set noDesfDesa
     *
     * @param string $noDesfDesa
     * @return Registry
     */
    public function setNoDesfDesa($noDesfDesa)
    {
        $this->noDesfDesa = $noDesfDesa;

        return $this;
    }

    /**
     * Get noDesfDesa
     *
     * @return string
     */
    public function getNoDesfDesa()
    {
        return $this->noDesfDesa;
    }

    /**
     * Set tiempoRcpa
     *
     * @param string $tiempoRcpa
     * @return Registry
     */
    public function setTiempoRcpa($tiempoRcpa)
    {
        $this->tiempoRcpa = $tiempoRcpa;

        return $this;
    }

    /**
     * Get tiempoRcpa
     *
     * @return string
     */
    public function getTiempoRcpa()
    {
        return $this->tiempoRcpa;
    }

    /**
     * Set noDesfRcpa
     *
     * @param string $noDesfRcpa
     * @return Registry
     */
    public function setNoDesfRcpa($noDesfRcpa)
    {
        $this->noDesfRcpa = $noDesfRcpa;

        return $this;
    }

    /**
     * Get noDesfRcpa
     *
     * @return string
     */
    public function getNoDesfRcpa()
    {
        return $this->noDesfRcpa;
    }

    /**
     * Set duracionRcp
     *
     * @param string $duracionRcp
     * @return Registry
     */
    public function setDuracionRcp($duracionRcp)
    {
        $this->duracionRcp = $duracionRcp;

        return $this;
    }

    /**
     * Get duracionRcp
     *
     * @return string
     */
    public function getDuracionRcp()
    {
        return $this->duracionRcp;
    }

    /**
     * Set otros
     *
     * @param string $otros
     * @return Registry
     */
    public function setOtros($otros)
    {
        $this->otros = $otros;

        return $this;
    }

    /**
     * Get otros
     *
     * @return string
     */
    public function getOtros()
    {
        return $this->otros;
    }

    /**
     * Set hcHospital
     *
     * @param string $hcHospital
     * @return Registry
     */
    public function setHcHospital($hcHospital)
    {
        $this->hcHospital = $hcHospital;

        return $this;
    }

    /**
     * Get hcHospital
     *
     * @return string
     */
    public function getHcHospital()
    {
        return $this->hcHospital;
    }

    /**
     * Set fechaAltaHospital
     *
     * @param \DateTime $fechaAltaHospital
     * @return Registry
     */
    public function setFechaAltaHospital($fechaAltaHospital)
    {
        $this->fechaAltaHospital = $fechaAltaHospital;

        return $this;
    }

    /**
     * Get fechaAltaHospital
     *
     * @return \DateTime
     */
    public function getFechaAltaHospital()
    {
        return $this->fechaAltaHospital;
    }

    /**
     * Set fechaEvolucion6Meses
     *
     * @param \DateTime $fechaEvolucion6Meses
     * @return Registry
     */
    public function setFechaEvolucion6Meses($fechaEvolucion6Meses)
    {
        $this->fechaEvolucion6Meses = $fechaEvolucion6Meses;

        return $this;
    }

    /**
     * Get fechaEvolucion6Meses
     *
     * @return \DateTime
     */
    public function getFechaEvolucion6Meses()
    {
        return $this->fechaEvolucion6Meses;
    }

    /**
     * Set fechaEvolucionAnio
     *
     * @param \DateTime $fechaEvolucionAnio
     * @return Registry
     */
    public function setFechaEvolucionAnio($fechaEvolucionAnio)
    {
        $this->fechaEvolucionAnio = $fechaEvolucionAnio;

        return $this;
    }

    /**
     * Get fechaEvolucionAnio
     *
     * @return \DateTime
     */
    public function getFechaEvolucionAnio()
    {
        return $this->fechaEvolucionAnio;
    }

    /**
     * ----------------------------------------------------------------------------------------------------------------
     * Métodos auxiliares
     * ----------------------------------------------------------------------------------------------------------------
     */

    /**
     * __toString()
     * @return string
     */
    public function __toString()
    {
        return $this->getNombre();
    }

    /**
     * toArray()
     *
     * Método para transformar una entidad en un array listo para enchufárselo a una response en json.
     *
     * Útil para devolver resultados paginados a los dataTables, para no tener que componer los arrays en los
     * controladores
     *
     * @return array
     */
    public function toArray()
    {
        //El método formatObject solamente está disponible a partir de php 5.5
        if (method_exists('IntlDateFormatter', 'formatObject')) {
            $dateStr = \IntlDateFormatter::formatObject($this->getFecha(), BaseUtil::getDateFormat());
        }
        else {
            $dateStr = $this->getFecha()->format('d-m-y');
        }

        return array(
            'id' => $this->getId(),
            'name' => $this->getNombre(),
            'date' => $dateStr,
            'age' => $this->getEdad(),
            'serviceCode' => $this->getCodigoServicio(),
            'serviceNumber' => $this->getNumeroServicio(),
        );
    }

}

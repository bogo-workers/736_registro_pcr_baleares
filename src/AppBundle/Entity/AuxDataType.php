<?php
/**
 * Created by PhpStorm.
 * User: carlos
 * Date: 20/03/15
 * Time: 18:16
 */

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\AuxDataTypeRepository")
 * @ORM\Table(name="auxdatatypes")
 */
class AuxDataType
{
    //Asignar todos los tipos de auxiliares a una constante que empiece por TYPE_
    //Además de consistencia, se usa al final para tener controlados los tipos de datos auxiliares que manejamos

    const TYPE_REGISTROPCR = 'registropcr';
    const TYPE_COMPLETO = 'completo';
    const TYPE_MOTIVOLLAMADA = 'motivollamada';
    const TYPE_UNIDAD = 'unidad';
    const TYPE_MEDICO = 'medico';
    const TYPE_DUE = 'due';
    const TYPE_REGULADOR = 'regulador';
    const TYPE_POBLACION = 'poblacion';
    const TYPE_SEXO = 'sexo';

    const TYPE_HTA = 'hta';
    const TYPE_DMNI = 'dmni';
    const TYPE_HIPERCOLESTEROL = 'hipercolesterol';
    const TYPE_FUMADOR = 'fumador';
    const TYPE_ENOLISMO = 'enolismo';
    const TYPE_COCAINA = 'cocaina';
    const TYPE_HIV = 'hiv';
    const TYPE_IAM = 'iam';
    const TYPE_DAI = 'dai';
    const TYPE_EPOC = 'epoc';
    const TYPE_AVC = 'avc';
    const TYPE_INSRENAL = 'insrenal';
    const TYPE_OBESIDAD = 'obesidad';
    const TYPE_NEOPLASIA = 'neoplasia';
    const TYPE_FAMILIA_MS = 'familiams';
    const TYPE_FAMILIA_CI = 'familiaci';
    const TYPE_CIRCARDIACA = 'circardiaca';

    const TYPE_LUGAR_PCR = 'lugarpcr';
    const TYPE_PRESENCIADA = 'presenciada';
    const TYPE_QUIEN = 'quien';
    const TYPE_TIPO_1ER_RCP = 'tipoprimerrcp';
    const TYPE_TIEMPO_1ER_DSF = 'tiempoprimerdsf';
    const TYPE_RCP_TELEFONICA = 'rcptelefonica';

    const TYPE_1ER_INTERVINIENTE = 'primerinterviniente';
    const TYPE_PRESENCIA_POLICIA = 'presenciapolicia';
    const TYPE_TIEMPO_RCPB = 'tiemporcpb';
    const TYPE_DESA = 'desa';
    const TYPE_RCP_AVANZADA = 'rcpavanzada';
    const TYPE_TIPO_RCPA = 'tiporcpa';
    const TYPE_RITMO_INICIAL = 'ritmoinicial';
    const TYPE_RITMO_SI_ROSC = 'ritmosirosc';
    const TYPE_GLASGOW_SI_ROSC = 'glasgowsirosc';

    const TYPE_VIA_AEREA = 'viaaerea';
    const TYPE_ACCESO_VENOSO = 'accesovenoso';
    const TYPE_CAPNOGRAFO = 'capnografo';
    const TYPE_HIPOTERMIA = 'hipotermia';
    const TYPE_MARCAPASOS = 'marcapasos';
    const TYPE_TROMBOLISIS = 'trombolisis';
    const TYPE_DRENAJE_TORACICO = 'drenajetoracico';
    const TYPE_PERICARDIOCENTESIS = 'pericardiocentesis';
    const TYPE_ADRENALINA = 'adrenalina';
    const TYPE_ATROPINA = 'atropina';
    const TYPE_AMIODARONA = 'amiodarona';
    const TYPE_BICARBONATO = 'bicarbonato';
    const TYPE_DROGAS = 'drogas';
    const TYPE_ELECTROLITOS = 'electrolitos';

    const TYPE_ETIOLOGIA = 'etiologia';
    const TYPE_SOSPECHA_ETIOLOGICA = 'sospechaetiologica';
    const TYPE_EXITUS = 'exitus';
    const TYPE_HOSPITAL_DESTINO = 'hospitaldestino';
    const TYPE_HIPOTERMIA_HOSP = 'hipotermiahosp';
    const TYPE_DAI2 = 'dai2';
    const TYPE_DONANTE = 'donante';
    const TYPE_LET = 'let';
    const TYPE_EVOLUCION_ALTA = 'evolucionalta';
    const TYPE_EVOLUCION_6_MESES = 'evolucion6meses';
    const TYPE_EVOLUCION_ANIO = 'evolucionanio';


    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="bigint", options={"unsigned":true})
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var ArrayObject
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\AuxData", mappedBy="type")
     */
    private $auxData;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", unique=true)
     */
    private $shortName;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $description;


    /**
     *
     */
    public function __construct()
    {
        $this->auxData = new ArrayCollection(); //Datos auxiliares asociados a este tipo
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set shortName
     *
     * @param string $name
     * @return AuxDataType
     */
    public function setShortName($name)
    {
        $this->shortName = $name;

        return $this;
    }

    /**
     * Get shortName
     *
     * @return null|string
     */
    public function getShortName()
    {
        return $this->shortName;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return AuxDataType
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Add auxData
     *
     * @param \AppBundle\Entity\AuxData $auxData
     * @return AuxDataType
     */
    public function addAuxDatum(AuxData $auxData)
    {
        $this->auxData[] = $auxData;

        return $this;
    }

    /**
     * Remove auxData
     *
     * @param \AppBundle\Entity\AuxData $auxData
     */
    public function removeAuxDatum(AuxData $auxData)
    {
        $this->auxData->removeElement($auxData);
    }

    /**
     * Get auxData
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAuxData()
    {
        return $this->auxData;
    }


    /**
     * ----------------------------------------------------------------------------------------------------------------
     * Métodos auxiliares
     * ----------------------------------------------------------------------------------------------------------------
     */
    public function __toString()
    {
        return $this->getDescription();
    }



    /**
     * ----------------------------------------------------------------------------------------------------------------
     * Métodos estáticos
     * ----------------------------------------------------------------------------------------------------------------
     */

    /**
     * Obtenemos los tipos de datos auxiliares a partir de las constantes de clase.
     * Cada constante de la clase que empiece por TYPE_ referencia un tipo de dato auxiliar.
     *
     * @return array
     */
    public static function getTypes()
    {
        $rc = new \ReflectionClass('AppBundle\Entity\AuxDataType');
        $constants = $rc->getConstants();   //Array de ctes de la clase. Nombre en la clave y valor en el valor

        //Devolvemos los valores de las ctes cuyo nombre empiece por TYPE_
        //No se puede hacer directamente con array_filter porque array_filter pasa los valores al callback, no las claves
        $matchedConstants = array_filter(array_keys($constants), function($c) {
            return strpos($c, 'TYPE_')===0;
        });
        return array_intersect_key($constants, array_flip($matchedConstants));
    }


    /**
     * Este método es un apaño para el tema de la separación de los tipos de datos auxiliares en bloques.
     * Son tantos tipos que tanto en el menú lateral como en el formulario de registros se dividen en bloques
     * para hacerlo más cómodo.
     * Es una división ficticia, a efectos de los tipos todos son iguales.
     * En este método le damos una descripción a cada bloque y agrupamos los tipos usando las ctes que los representan.
     *
     * @return array
     */
    public static function getBlocks()
    {
        return array(
            array(
                'description' => 'Demográficos',
                'types' => array(self::TYPE_REGISTROPCR, self::TYPE_COMPLETO, self::TYPE_MOTIVOLLAMADA, self::TYPE_UNIDAD, self::TYPE_MEDICO, self::TYPE_DUE, self::TYPE_REGULADOR, self::TYPE_POBLACION, self::TYPE_SEXO),
            ),
            array(
                'description' => 'Antecedentes',
                'types' => array(self::TYPE_HTA, self::TYPE_DMNI, self::TYPE_HIPERCOLESTEROL, self::TYPE_FUMADOR, self::TYPE_ENOLISMO, self::TYPE_COCAINA, self::TYPE_HIV, self::TYPE_IAM, self::TYPE_DAI, self::TYPE_EPOC, self::TYPE_AVC, self::TYPE_INSRENAL, self::TYPE_OBESIDAD, self::TYPE_NEOPLASIA, self::TYPE_FAMILIA_MS, self::TYPE_FAMILIA_CI, self::TYPE_CIRCARDIACA),
            ),
            array(
                'description' => 'Localización',
                'types' => array(self::TYPE_LUGAR_PCR, self::TYPE_PRESENCIADA, self::TYPE_QUIEN, self::TYPE_TIPO_1ER_RCP, self::TYPE_TIEMPO_1ER_DSF, self::TYPE_RCP_TELEFONICA),
            ),
            array(
                'description' => 'RCP',
                'types' => array(self::TYPE_1ER_INTERVINIENTE, self::TYPE_PRESENCIA_POLICIA, self::TYPE_TIEMPO_RCPB, self::TYPE_DESA, self::TYPE_RCP_AVANZADA, self::TYPE_TIPO_RCPA, self::TYPE_RITMO_INICIAL, self::TYPE_RITMO_SI_ROSC, self::TYPE_GLASGOW_SI_ROSC),
            ),
            array(
                'description' => 'Tratamiento',
                'types' => array(self::TYPE_VIA_AEREA, self::TYPE_ACCESO_VENOSO, self::TYPE_CAPNOGRAFO, self::TYPE_HIPOTERMIA, self::TYPE_MARCAPASOS, self::TYPE_TROMBOLISIS, self::TYPE_DRENAJE_TORACICO, self::TYPE_PERICARDIOCENTESIS, self::TYPE_ADRENALINA, self::TYPE_ATROPINA, self::TYPE_AMIODARONA, self::TYPE_BICARBONATO, self::TYPE_DROGAS, self::TYPE_ELECTROLITOS),
            ),
            array(
                'description' => 'Evolución',
                'types' => array(self::TYPE_ETIOLOGIA, self::TYPE_SOSPECHA_ETIOLOGICA, self::TYPE_EXITUS, self::TYPE_HOSPITAL_DESTINO, self::TYPE_HIPOTERMIA_HOSP, self::TYPE_DAI2, self::TYPE_DONANTE, self::TYPE_LET, self::TYPE_EVOLUCION_ALTA, self::TYPE_EVOLUCION_6_MESES, self::TYPE_EVOLUCION_ANIO),
            ),
        );
    }

}

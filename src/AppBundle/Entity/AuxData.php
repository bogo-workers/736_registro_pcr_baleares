<?php
/**
 * Created by PhpStorm.
 * User: carlos
 * Date: 20/03/15
 * Time: 18:16
 */

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\AuxDataRepository")
 * Pueden existir elementos que se llamen de la misma forma, pero siempre que sean de tipos diferentes.
 * Definimos aquí entonces la constraint de unicidad compuesta, así que NO poner las columnas a unique.
 * Esto crea un índice compuesto por las dos columnas.
 * @ORM\Table(name="auxdata", uniqueConstraints={@ORM\UniqueConstraint(name="unique_name_in_type", columns={"type_id","name"})})
 */
class AuxData
{
    const STATE_ENABLED = true;
    const STATE_DISABLED = false;

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="bigint", options={"unsigned":true})
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\AuxDataType", inversedBy="auxData")
     * @ORM\JoinColumn(name="type_id", referencedColumnName="id")
     */
    private $type;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @var int
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $state = true;  //Inicializada con el valor por defecto que queramos ver en el formulario


    /**
     * @var
     *
     * @ORM\ManyToMany(targetEntity="Registry", mappedBy="auxData")
     */
    private $registries;


    public function __construct()
    {
        $this->registries = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return AuxData
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @param \AppBundle\Entity\AuxDataType $type
     * @return AuxData
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return \AppBundle\Entity\AuxDataType
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get state.
     *
     * @return bool
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->getState()==self::STATE_ENABLED;
    }

    /**
     * Set state.
     *
     * @param bool $state
     * @return AuxData
     */
    public function setState($state)
    {
        $this->state = $state;
        return $this;
    }

    /**
     * Get registries
     * No hay setters (addRegistry - removeRegistry). No quiero que desde datos auxiliares se
     * puedan añadir/quitar registros. Aún así la relación la quiero bidireccional para tener
     * este getRegistries y comprobar fácilmente si un dato auxiliar tiene registros asociados
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRegistries()
    {
        return $this->registries;
    }


    /**
     * ----------------------------------------------------------------------------------------------------------------
     * Métodos auxiliares
     * ----------------------------------------------------------------------------------------------------------------
     */

    /**
     * __toString()
     * @return string
     */
    public function __toString()
    {
        return $this->getName();
    }

    /**
     * toArray()
     *
     * Método para transformar una entidad en un array listo para enchufárselo a una response en json.
     *
     * Útil para devolver resultados paginados a los dataTables, para no tener que componer los arrays en los
     * controladores
     *
     * @return array
     */
    public function toArray()
    {
        return array(
            'id' => $this->getId(),
            'name' => $this->getName(),
            'active' => $this->isActive(),
        );
    }

}

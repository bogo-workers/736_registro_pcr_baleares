<?php
/**
 * Created by PhpStorm.
 * User: carlos
 * Date: 07/04/15
 * Time: 00:38
 */

namespace AppBundle\Helper;

use AppBundle\Util\BaseUtil;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Router;
use Symfony\Component\Translation\Translator;

use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;

/**
 * Helper class of Breadcrumb
 */
class BreadcrumbsHelper
{

    /**
     * Static function that prepares the breadcrumb for breadcrumbListener
     *
     * @param Router $router
     * @param EntityManager $doctrine
     * @param Translator $tr
     * @param Breadcrumbs $whiteOctoberBreadcrumbs
     * @param Request $request
     */
    public static function setBreadcrumb(Router $router, EntityManager $doctrine, Translator $tr, Breadcrumbs $whiteOctoberBreadcrumbs, Request $request)
    {
        $routeInfo = $router->matchRequest($request);
        $routeName = $routeInfo['_route'];

        //Funciones de callback para pasarle al generador del breadcrumbs
        $fGetLabel = function($o) use ($tr) {
            return $o->label;
        };
        $fGetRoute = function($o) use ($router) {
            if (isset($o->route)) {
                return $router->generate($o->route, isset($o->params)?(array)$o->params:array());
            }
            return '';
        };

        //Objetos que forman el menú

        //Inicio. Siempre está
        $path = array(
            BaseUtil::arrayToObject(array(
                'label' => 'Inicio',
                'route' => 'bogo_backnet_default_index',
            ))
        );

        //Si estamos con los datos auxiliares
        if (strpos($routeName, 'bogo_backnet_auxdata_') === 0) {
            //Si tenemos un id es que estamos editando/eliminando. A la ruta anterior (listado) hay que pasarle el tipo
            if (isset($routeInfo['id'])) {
                $auxData = $doctrine->getRepository('AppBundle:AuxData')->find($routeInfo['id']);
                if ($auxData) $type = $auxData->getType()->getShortName();
            }
            $path[] =
                BaseUtil::arrayToObject(
                    array(
                        'label' => 'Listado de datos auxiliares',
                        'route' => 'bogo_backnet_auxdata_list',
                        'params' => isset($type)?array('type'=>$type):array(),
                    )
                );
            switch ($routeName) {
                case 'bogo_backnet_auxdata_add':
                    //$path[] = BaseUtil::arrayToObject(array('label' => $tr->trans('Añadir nuevo dato auxiliar')));
                    $path[] = BaseUtil::arrayToObject(array('label' => 'Añadir nuevo dato auxiliar'));
                    break;
                case 'bogo_backnet_auxdata_edit':
                    //$path[] = BaseUtil::arrayToObject(array('label' => $tr->trans('Editar dato auxiliar existente')));
                    $path[] = BaseUtil::arrayToObject(array('label' => 'Editar dato auxiliar existente'));
                    break;
                case 'bogo_backnet_auxdata_delete':
                    //$path[] = BaseUtil::arrayToObject(array('label' => $tr->trans('Eliminar dato auxiliar existente')));
                    $path[] = BaseUtil::arrayToObject(array('label' => 'Eliminar dato auxiliar existente'));
                    break;
            }
        }
        //Si estamos con los registros
        elseif (strpos($routeName, 'bogo_backnet_registry_') === 0) {
            //Esta es más fácil. El listado de registros no lleva argumentos
            $path[] =
                BaseUtil::arrayToObject(
                    array(
                        'label' => 'Listado de registros',
                        'route' => 'bogo_backnet_registry_list',
                    )
                );
            switch ($routeName) {
                case 'bogo_backnet_registry_add':
                    $path[] = BaseUtil::arrayToObject(array('label' => 'Añadir nuevo registro'));
                    break;
                case 'bogo_backnet_registry_edit':
                    $path[] = BaseUtil::arrayToObject(array('label' => 'Editar registro existente'));
                    break;
                case 'bogo_backnet_registry_delete':
                    $path[] = BaseUtil::arrayToObject(array('label' => 'Eliminar registro existente'));
                    break;
            }
        }
        //Si estamos con los usuarios
        elseif (strpos($routeName, 'bogo_users_') === 0) {
            $path[] =
                BaseUtil::arrayToObject(
                    array(
                        'label' => 'Listado de usuarios',
                        'route' => 'bogo_users_list',
                    )
                );
            switch ($routeName) {
                case 'bogo_users_add':
                    $path[] = BaseUtil::arrayToObject(array('label' => 'Añadir nuevo usuario'));
                    break;
                case 'bogo_users_edit':
                    $path[] = BaseUtil::arrayToObject(array('label' => 'Editar usuario existente'));
                    break;
                case 'bogo_users_delete':
                    $path[] = BaseUtil::arrayToObject(array('label' => 'Eliminar usuario existente'));
                    break;
            }
        }

        //Pasamos lo configurado al breadcrumbs
        $whiteOctoberBreadcrumbs->addObjectArray($path, $fGetLabel, $fGetRoute);
    }

}
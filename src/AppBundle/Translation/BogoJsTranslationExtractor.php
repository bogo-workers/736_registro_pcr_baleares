<?php
/**
 * Created by PhpStorm.
 * User: carlos
 * Date: 25/03/15
 * Time: 10:57
 */

/**
 * Este servicio es un extractor de cadenas traducibles para ficheros Javascript.
 * Utilizando JMSTranslationBundle podemos hacer que utilice este servicio para recorrer
 * ficheros js, obtener las cadenas a traducir y guardarlas en los ficheros xliff igual
 * que se hace con el PHP o Twig.
 *
 * El servicio está definido en config/services.yml y está parametrizado con la cadena de
 * apertura y cierre entre las que hay que buscar. Por el momento solamente acepta una.
 * Se puede mejorar el servicio para poder especificar varias diferentes, no sería complicado.
 *
 */
namespace AppBundle\Translation;

use JMS\TranslationBundle\Translation\Extractor\FileVisitorInterface;
use JMS\TranslationBundle\Model\Message;
use JMS\TranslationBundle\Model\MessageCatalogue;
use JMS\TranslationBundle\Model\FileSource;

class BogoJsTranslationExtractor implements FileVisitorInterface
{
    private $domain;
    private $tags;

    /**
     * @param $openTag
     * @param $closeTag
     */
    public function __construct($domain="javascripts", $tags)
    {
        $this->domain = $domain;
        $this->tags = $tags;
    }

    /**
     * Este es el método que recorre los ficheros y va guardando las extracciones
     *
     * @param \SplFileInfo $file
     * @param MessageCatalogue $catalogue
     */
    public function visitFile(\SplFileInfo $file, MessageCatalogue $catalogue)
    {
        //Primero comprobamos si es un fichero javascript
        if ('.js' !== substr($file, -3)) {
            return;
        }
        //Cargamos su contenido
        $fullFile = file_get_contents($file);
        if (null === $fullFile || empty($fullFile)) {
            return;
        }

        //Seguro que esto es mejorable, pero como primera versión funciona. Busco la cadena a extraer
        //entre las cadenas de apertura y cierre. Si la encuentro, la guardo en el catálogo y borro del
        //contenido del fichero el conjunto de apertura+extraida+cierre (así no la vuelvo a encontrar).
        foreach ($this->getTags() as $tag) {    //Para cada par de tags de apertura-cierre
            $content = $fullFile;
            do {
                //Obtengo la cadena a extraer
                $msg = self::getStringBetween($content, $tag['open'], $tag['close']);
                if (!empty($msg)) {
                    //Guardo en el catálogo con el dominio especificado
                    $message = new Message($msg, $this->getDomain());
                    $message->addSource(new FileSource((string)$file));
                    $catalogue->add($message);

                    //$s contiene apertura+extraida+cierre
                    $s = $tag['open'] . $msg . $tag['close'];
                    //Recorto content para no volver a encontrar lo mismo
                    $content = substr($content, strpos($content, $s) + strlen($s));
                }
            } while (!empty($msg)); //Repito el proceso hasta que no encuentre ninguna cadena
        }
    }


    /** Estos dos métodos los exige el interface FileVisitorInterface */
    public function visitPhpFile(\SplFileInfo $file, MessageCatalogue $catalogue, array $ast) { }
    public function visitTwigFile(\SplFileInfo $file, MessageCatalogue $catalogue, \Twig_Node $node) { }
    /** */


    private function getDomain()
    {
        return $this->domain;
    }
    private function getTags()
    {
        return $this->tags;
    }


    /**
     * Este método es el encargado de buscar en $string la cadena contenida entre $start y $end y devolverla
     *
     * @param $string
     * @param $start
     * @param $end
     * @return string
     */
    private static function getStringBetween($string, $start, $end)
    {
        $string = " ".$string;
        $ini = strpos($string,$start);
        if ($ini == 0) return "";
        $ini += strlen($start);
        $len = strpos($string,$end,$ini) - $ini;

        return substr($string,$ini,$len);
    }
}
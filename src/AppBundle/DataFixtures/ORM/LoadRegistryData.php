<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\AuxData;
use AppBundle\Entity\AuxDataType;
use AppBundle\Entity\Registry;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadRegistryData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        /*
        $entity = new Registry();
        $manager->persist($entity);

        $entity->setName('REGISTRY01');
        $entity->setDate(new \DateTime('now'));
        $entity->setAge(50);
        $entity->setServiceCode('SRV_CODE01');
        $entity->setServiceNumber('SRV_NUMBER01');
        $entity->setOschar(true);

        $entity->setRegistroPcr($this->getReference('auxData_registropcr'));
        $entity->setCompleto($this->getReference('auxData_completo'));
        //$entity->addAuxDatum($this->getReference('auxData_registropcr'));
        //$entity->addAuxDatum($this->getReference('auxData_completo'));

        //dump($entity->getRegistroPcr()->getName());
        //dump($entity->getCompleto()->getName());

        $manager->flush();
        */
    }

    public function getOrder()
    {
        return 30; //Ha de ejecutarse esta fixture después de la de AuxData
    }
}
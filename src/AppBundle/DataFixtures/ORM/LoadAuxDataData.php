<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\AuxData;
use AppBundle\Entity\AuxDataType;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadAuxDataData extends AbstractFixture implements OrderedFixtureInterface
{
    private $em;    //Doctrine

    public function load(ObjectManager $manager)
    {
        $this->em = $manager;

        /* PRIMER BLOQUE */
        $this->addAuxData(array('PCR','HIPOTERMIA','AMBOS','NINGUNO'), AuxDataType::TYPE_REGISTROPCR); //Registro pcr
        $this->addAuxData(array('SI', 'NO', 'NO CCUM','NO ALFA','NO TEM','NO CCUM/ALFA','SEGUIMIENTO'), AuxDataType::TYPE_COMPLETO); //Completo
        $this->addAuxData(array('Varón','Mujer','Desconocido'), AuxDataType::TYPE_SEXO); //Sexo
        $this->addAuxData(array('Alaró','Alcudia','Algaida','Andratx','Ariany','Artá','Banyalbufar','Binissalem','Buger','Bunyola','Calvia','Campanet','Campos', 'Can Picafort', 'Capdepera','Consell','Costitx','Escorca','Esporles','Estellencs','Felanitx','Fornalutx','Inca','Lloret','Lloseta','Llubí','Llucmajor','Manacor','Mancor','María','Marratxi','Montuiri','Muro','Palma','Petra','Pollença','Porreres','Puigpuyent','Sa Pobla','Sant Joan','Sant Llorenç','Santa Eugenia','Santa Margalida','Santa María','Santanyi','Selva','Sencelles','Ses Salines','Sineu','Soller','Son Servera','Valldemossa','Villafranca','Ibiza (IB)','Sant Josep de Sa Talaia (IB)','Sant Antoni de Portmany (IB)','Santa Eulalia des Riu (IB)','Sant Joan de Llabritja (IB)','San Rafael (IB)','Formentera','Ciudatella (Me)','Ferreries (Me)','Es Migjorn (Me)','Es Mercadal (Me)','Alaior (Me)','Maó (Me)','Es Castell (Me)','Sant Lluis (Me)','Desconocido'), AuxDataType::TYPE_POBLACION); //Poblacion
        $this->addAuxData(array('PCR','Inconsciente-coma','Dolor torácico','Disnea','Síncope','Evento traumático','Ahogamiento','Síntomas inespecíficos','Palpitaciones','Convulsiones','Otros','Atragantamiento'), AuxDataType::TYPE_MOTIVOLLAMADA); //Motivo llamada
        $this->addAuxData(array('A211','A212','A213','A214','A221','A222','A231','A241','A242','A251','A252','A261','AV811','AV841','HL851','PAC','TEM','Viandante','Desconocido'), AuxDataType::TYPE_UNIDAD); //Unidad
        $this->addAuxData(array('PAC', 'TEM', 'Abad, Cristina', 'Albert, Gustavo', 'Alomar, Lorenzo', 'Alonso, Carla', 'Alonso, Juan', 'Álvarez, Chema', 'Amigo, Pablo', 'Avila, Gabriela', 'Barbero, Marisa', 'Bertrán, Itziar', 'Bickic, Esteban', 'Bisquerra, Dolores', 'Blanco, Julio', 'Borras, Jaime', 'Campos, Xisco', 'Castell, Fco Javier', 'Comas, Antonio', 'Corradini, Daniel', 'Crespo, Angel', 'Cuadrado, Carlos', 'D´Eufemia, Sandra', 'De Arce, Cristina', 'De la Cal, Santiago', 'Diez, Inmaculada', 'Escoda, Cosme', 'Estarellas, Juan', 'Fernández, David', 'Fernández, Ismael', 'Ferrer, Juan', 'Florez, Francisco', 'Gago, Carlos', 'Generelo, Marta', 'Gisbert, Javier', 'Gómez, Fernándo', 'Gomila, Jaume', 'González, Anselmo', 'Gonzalez, Elena', 'Grajera, Yolanda', 'Jaume, Jimmy', 'Juan, Toni', 'Karin', 'Lamoso, Javier', 'Laque, Estibaliz', 'Lerma, Enrique', 'Llorca, Antonio', 'López, Isabel', 'Malandrich, Jaume', 'Martín, Raul', 'Matamoro, Joaquín', 'Monje, Germán', 'Morán, Iñigo', 'Morey, Toni', 'Moros, Albert', 'Morós, Ruth', 'Munar, Pere', 'Oliver, Eduardo', 'Recio, Josep', 'Revuelta, Carlos', 'Roca, Sergio', 'Rovira, Ramón', 'Royo, Neus', 'Rubí, Ines', 'Rubio, Pilar', 'Sahuquillo, Miguel Angel', 'San Segundo, Marta', 'Sánchez, Cristina', 'Sánchez, Juan Luis', 'Santaliestra, Jesús', 'Santiago, Eva', 'Santiso, Rafael', 'Sara, Javier', 'Tomás, Miguel', 'Tronconi, Oscar', 'Unzaga, Iñaki', 'Vita, Gerardo', 'Zarzar, Jorge'), AuxDataType::TYPE_MEDICO); //Medicos
        $this->addAuxData(array('Alonso, Francisco', 'Aloy, M Jose', 'Alvarez, Jose', 'Alvarez, Olga', 'Aparicio, Juan Miguel', 'Autonell, Toni', 'Berg, Talitha', 'Bestard, Carol', 'Blanco, Jenifer', 'Blanco, Susana', 'Bregel, Maria', 'Cabot, Aina', 'Cantos, Cristina', 'Carbajosa, Dafne', 'Castillo, Fco Javier', 'Cereceda,Francisco', 'Chivite, Inma', 'Coll, Marga', 'Coronel, Cinta', 'Cucarella', 'Delas, Daniel', 'Ferrer, Vicky', 'Garcia, Ana', 'Garcia, Andres', 'Garcia, Angel', 'Garcia, Francisco', 'Garcia, Juanjo', 'Garcia, Melchor', 'Garriga, Angels', 'Gili, Andreu', 'González, Jose Luis', 'Grech, Isabel', 'Hernandez, Erica', 'Hernandez, Rafa', 'Höster, Carlos',  'Iglesias, Dolores', 'Juan, Toni', 'Lax, Luis', 'Leal, Estrella', 'Lombo, Elena', 'Lopez, Juani', 'Mariscal, Pilar', 'Maya, Dolors', 'Migoya, M Carmen', 'Moliner, Laia', 'Montero, Pedro', 'Muñoz, Virginia', 'Olmo, Paqui', 'Palma, Sonia', 'Perello, Cati', 'Pericas, Marga', 'Peruga', 'Pons, Jaume', 'Pozo, Carmen', 'Pulet, Jose Luis', 'Ribas, Neus', 'Rios Albert', 'Rodríguez, Lucía', 'Roman, Pascual', 'Romero, Marcos', 'Ruiz, Ana', 'Salguero, Felipe', 'Salicru, Joan', 'Salom, Conchi', 'Sanchez, Toñi', 'Urendez, Ana', 'Vallori, Cati', 'Vega, Nuria', 'Venteo, Charo', 'Villalonga, Marga', 'Zerdoumi, Farida', 'Zuazaga, Neomi'), AuxDataType::TYPE_DUE); //DUE
        $this->addAuxData(array('Ancuta, Mihaela', 'Bestad, Maria', 'Canals, Isabel', 'Ceniceros, Isabel', 'Diaz, Toñi', 'Finozzi, Wilson', 'Gonzalez, Elena', 'Hernandez, Ernesto', 'Hidalgo, Lupe', 'Juste, Isabel', 'Mir, Marga', 'Olivera, Grettel', 'Revuelta, Carlos', 'Riart, Dolres', 'Rubí, Ines', 'Rotger, Antonia', 'Sara, Javier', 'Tronconi, Oscar', 'Vita, Gerardo', 'Otro', 'Desconocido'), AuxDataType::TYPE_REGULADOR); //Regulador

        /* SEGUNDO BLOQUE */
        $this->addAuxData(array('HTA', 'NO', 'DESCONOCIDO'), AuxDataType::TYPE_HTA); //HTA
        $this->addAuxData(array('DM', 'DMNID', 'DMID', 'NO', 'DESCONOCIDO'), AuxDataType::TYPE_DMNI); //DMNI
        $this->addAuxData(array('COL', 'TG', 'DISLIPEMIA', 'NO', 'DESCONOCIDO'), AuxDataType::TYPE_HIPERCOLESTEROL); //Dislipemia
        $this->addAuxData(array('FUM', 'EXFUM', 'NO', 'DESCONOCIDO'), AuxDataType::TYPE_FUMADOR); //Fumador
        $this->addAuxData(array('ENOL', 'NO', 'DESCONOCIDO'), AuxDataType::TYPE_ENOLISMO); //Enolismo
        $this->addAuxData(array('COCA', 'ADVP', 'AMBOS', 'NO', 'DESCONOCIDO'), AuxDataType::TYPE_COCAINA); //Cocaina
        $this->addAuxData(array('HIV', 'NO', 'DESCONOCIDO'), AuxDataType::TYPE_HIV); //HIV
        $this->addAuxData(array('CARDIOPATA', 'ANGOR', 'IAM', 'FA+CARDIOPATIA', 'FA+IAM', 'NO', 'DESCONOCIDO'), AuxDataType::TYPE_IAM);
        $this->addAuxData(array('DAI', 'MCP', 'AMBOS', 'NO', 'DESCONOCIDO'), AuxDataType::TYPE_DAI); //DAI
        $this->addAuxData(array('EPOC', 'ASMA', 'NO', 'DESCONOCIDO'), AuxDataType::TYPE_EPOC); //Respiratorio
        $this->addAuxData(array('AVC', 'DISCAPACIDAD', 'NO', 'DESCONOCIDO'), AuxDataType::TYPE_AVC); //AVC
        $this->addAuxData(array('IR', 'DIALISIS', 'NO', 'DESCONOCIDO'), AuxDataType::TYPE_INSRENAL); //Renal
        $this->addAuxData(array('OBESO', 'NO', 'DESCONOCIDO'), AuxDataType::TYPE_OBESIDAD); //Obesidad
        $this->addAuxData(array('NEO', 'NO', 'DESCONOCIDO'), AuxDataType::TYPE_NEOPLASIA); //Neoplasia
        $this->addAuxData(array('FAM MS', 'NO', 'DESCONOCIDO'), AuxDataType::TYPE_FAMILIA_MS); //Familia MS
        $this->addAuxData(array('FAM CI', 'NO', 'DESCONOCIDO'), AuxDataType::TYPE_FAMILIA_CI); //Familia CI
        $this->addAuxData(array('BYPASS', 'VALVULAR', 'NO', 'DESCONOCIDO'), AuxDataType::TYPE_CIRCARDIACA); //CCV

        /* TERCER BLOQUE */
        $this->addAuxData(array('DOMICILIO', 'VIA PUBLICA', 'EDIFICIO PUBLICO', 'CENTRO SANITARIO', 'RESIDENCIA ASISTIDA', 'DENTRO DEPORTIVO', 'CENTRO COMERCIAL', 'AMB SVB', 'AMB SVA', 'PLAYA', 'PISCINA', 'HOTEL', 'TRANS MARITIMO', 'TRANS AEREO', 'AEROPUERTO', 'TRANS PUBLICO', 'CENTRO DOCENTE', 'OTROS', 'DESCONOCIDO'), AuxDataType::TYPE_LUGAR_PCR);
        $this->addAuxData(array('SI', 'NO', 'NO CONSTA'), AuxDataType::TYPE_PRESENCIADA);
        $this->addAuxData(array('FAMILIA', 'TESTIGO', 'SANI EMERGENCIAS', 'SANI NO EMERGENCIAS', 'TEM', 'PL', 'OTRA POLICIA', 'BOMBEROS', 'SOCORRISTAS', 'OTROS SERV PUBLICOS', 'OTRO', 'DESCONOCIDO'), AuxDataType::TYPE_QUIEN);
        $this->addAuxData(array('SBV por tfno', 'SVB masaje', 'SVB masaje + vent', 'SVB + DESA', 'SVA'), AuxDataType::TYPE_TIPO_1ER_RCP);
        $this->addAuxData(array('NO PRECISA DSF', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '>30 MIN RCP LARGA', '>30 MIN FALTA RECURSOS'), AuxDataType::TYPE_TIEMPO_1ER_DSF);
        $this->addAuxData(array('SI', 'SI (medico ?)', 'NO', 'NO CONSTA', 'PRESENT SVB/SVA', 'Ancuta, Mihaela', 'Bestad, Maria', 'Canals, Isabel', 'Ceniceros, Isabel', 'Diaz, Toñi', 'Finozzi, Wilson', 'Gonzalez, Elena', 'Hernandez, Ernesto', 'Hidalgo, Lupe', 'Juste, Isabel', 'Mir, Marga', 'Olivera, Grettel', 'Revuelta, Carlos', 'Riart, Dolres', 'Rotger, Antonia', 'Rubí, Ines', 'Sara, Javier', 'Tronconi, Oscar', 'Vita, Gerardo', 'Otro'), AuxDataType::TYPE_RCP_TELEFONICA);

        /* CUARTO BLOQUE */
        $this->addAuxData(array('FAMILIA', 'VIANDANTE', 'PL', 'OTRA POLICIA', 'BOMBERO', 'OTRO SERV PUBLICO', 'TEM', 'PAC', 'SANITARIO', 'SOCORRISTA', 'CUALIFICA', 'SVA', 'OTRO', 'DESCONOCIDO', 'NO'), AuxDataType::TYPE_1ER_INTERVINIENTE);
        $this->addAuxData(array('POLICIA LOCAL', 'CNP', 'GUARDIA CIVIL', 'NO', 'DESCONOCIDO'), AuxDataType::TYPE_PRESENCIA_POLICIA);
        $this->addAuxData(array('NO RCP B', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '30-35', '35-40', '40-45', '45-50', '50-55', '55-60', 'MAS DE 60'), AuxDataType::TYPE_TIEMPO_RCPB);
        $this->addAuxData(array('PL', 'TEM', 'SOCORRISTA', 'VIANDANTE', 'LABORAL', 'OTRO', 'SI', 'NO', 'NO CONSTA', 'NO SVB', 'PAC'), AuxDataType::TYPE_DESA);
        $this->addAuxData(array('NO', 'AMB 061', 'PAC', 'ANESTESISTA', 'INTENSIVISTA', 'OTRO RECURSO'), AuxDataType::TYPE_RCP_AVANZADA);
        $this->addAuxData(array('SI', 'FUTIL', 'CIUDADOS POSTERIORES', 'NO INDICADA', 'DESCONOCIDO', 'NO'), AuxDataType::TYPE_TIPO_RCPA);
        $this->addAuxData(array('ASISTOLIA', 'DEM', 'FV', 'TVSP', 'BRADICARDIA EXTREMA', 'FV grado fino', 'DESCONOCIDO', 'MARCAPASOS', 'PARO RESPIRATORIO', 'SINUSAL'), AuxDataType::TYPE_RITMO_INICIAL);
        $this->addAuxData(array('SINUSAL', 'NODAL', 'BLOQUEO', 'TV', 'TPSV', 'AcxFA', 'FLUTER', 'OTRO', 'NO CONSTA'), AuxDataType::TYPE_RITMO_SI_ROSC);
        $this->addAuxData(array('3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', 'SEDADO', 'DESCONOCIDO'), AuxDataType::TYPE_GLASGOW_SI_ROSC);

        /* QUINTO BLOQUE */
        $this->addAuxData(array('IOT', 'FASTRACH', 'LMA', 'BOLSA-MASCARILLA', 'AIRTRACK', 'CRICOTIROIDECTOMIA', 'OTRO', 'NO PRECISA', 'DESCONOCIDO'), AuxDataType::TYPE_VIA_AEREA);
        $this->addAuxData(array('PERIFERICA', 'CENTRAL', 'INTRAOSEA', 'PERI + INTRAOSEA', 'PERI + CENTRAL', 'CENTRAL + INTRAOSEA', 'TODAS', 'NO', 'DESCONOCIDO'), AuxDataType::TYPE_ACCESO_VENOSO);
        $this->addAuxData(array('CAPNOGRAFO', 'NO', 'DESCONOCIDO'), AuxDataType::TYPE_CAPNOGRAFO);
        $this->addAuxData(array('HIPOTERMIA EH', 'NO INDICADA', 'MAL INDICADA', 'NO', 'NO ROSC', 'DESCONOCIDO'), AuxDataType::TYPE_HIPOTERMIA);
        $this->addAuxData(array('MARCAPASOS', 'NO', 'DESCONOCIDO'), AuxDataType::TYPE_MARCAPASOS);
        $this->addAuxData(array('TROMBOLISIS', 'NO', 'DESCONOCIDO', 'ACTP 1º'), AuxDataType::TYPE_TROMBOLISIS);
        $this->addAuxData(array('DREN TORACICO', 'NO', 'DESCONOCIDO'), AuxDataType::TYPE_DRENAJE_TORACICO);
        $this->addAuxData(array('PERICARDIO', 'NO', 'DESCONOCIDO'), AuxDataType::TYPE_PERICARDIOCENTESIS);
        $this->addAuxData(array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', 'MAS 10', 'DESCONOCE'), AuxDataType::TYPE_ADRENALINA);
        $this->addAuxData(array('ASISTOLIA', 'BRADICARDIA', 'BLOQUEO', 'OTRO', 'NO', 'DESCONOCE'), AuxDataType::TYPE_ATROPINA);
        $this->addAuxData(array('1 BOLO', '2 BOLOS', 'BOLO + PERFUSION', 'PERFUSIÓN', 'NO INDICADA', 'MAL INDICADA', 'POSTERIOR', 'NO', 'DESCONOCIDA', 'LIDOCAINA'), AuxDataType::TYPE_AMIODARONA);
        $this->addAuxData(array('SI', 'NO', 'DESCONOCE'), AuxDataType::TYPE_BICARBONATO);
        $this->addAuxData(array('NORADRENALINA', 'DOPAMINA', 'DOBUTAMINA', 'NORA + DOPA', 'NORA + DOBUTA', 'NORA +DOPA + DBT', 'DOPA +DBT', 'ADRENALINA', 'ADRENALINA + OTRA', 'OTRA', 'NO', 'DESCONOCIDO'), AuxDataType::TYPE_DROGAS);
        $this->addAuxData(array('CLCa', 'SulfMg', 'AMBOS', 'NINGUNO', 'DESCONOCIDO', 'OTRO'), AuxDataType::TYPE_ELECTROLITOS);
        
        /* SEXTO BLOQUE */
        $this->addAuxData(array('CARDIOLOGICO', 'NO CARDIO', 'TRAUMATOLOGICO', 'IN SITU', 'OTROS', 'DESCONOCIDO'), AuxDataType::TYPE_ETIOLOGIA);
        $this->addAuxData(array('SCA', 'ANEURISMA', 'INSUFICIENCIA CARDIACA', 'INSUFICIENCIA RESPIRATORIA', 'SEPSIS', 'NEUROLOGICO', 'TEP', 'INTOX FARMACOS', 'INTOX HUMO', 'INTOX DROGAS', 'NEUMOTORAX', 'ESTATUS ASMATICO', 'INMERSIÓN', 'OBSTRUCCIÓN VÍA AÉREA', 'ASPIRACIÓN', 'SANGRADO', 'TAPONAMIENTO CARDIACO', 'MUERTE SÚBITA LACTANTE', 'TCE GRAVE', 'DESCONOCIDO', 'OTROS'), AuxDataType::TYPE_SOSPECHA_ETIOLOGICA);
        $this->addAuxData(array('IN SITU', 'TRASLADO', 'URGENCIAS', 'NO'), AuxDataType::TYPE_EXITUS);
        $this->addAuxData(array('SON ESPASES', 'SON LLATZER', 'INCA', 'MANACOR', 'MATEU ORFILA', 'CAN MISSES', 'PALMAPLANAS', 'JUANEDA', 'ROTGER', 'POLICLINICA MIRAMAR', 'MURO', 'NUESTRA SEÑORA ROSARIO (IBIZA)', 'JUANEDA (MENORCA)', 'SON VERI', 'LLEVANT'), AuxDataType::TYPE_HOSPITAL_DESTINO);
        $this->addAuxData(array('HIPOTERMIA HOSP', 'NO', 'DESCONOCIDO'), AuxDataType::TYPE_HIPOTERMIA_HOSP);
        $this->addAuxData(array('DAI', 'NO', 'DESCONOCIDO'), AuxDataType::TYPE_DAI2);
        $this->addAuxData(array('DONANTE', 'NO', 'DESCONOCIDO'), AuxDataType::TYPE_DONANTE);
        $this->addAuxData(array('LET', 'NO', 'DESCONOCIDO'), AuxDataType::TYPE_LET);
        $this->addAuxData(array('OPC1', 'OPC1 Buena capac', 'OPC2', 'OPC2 moderada incap', 'OPC3', 'OPC3 severa incap', 'OPC4', 'OPC4 coma', 'OPC5', 'OPC5 Muerte', 'Vivo estado neuro desconocido', 'Desconocido'), AuxDataType::TYPE_EVOLUCION_ALTA);
        $this->addAuxData(array('OPC1', 'OPC1 Buena capac', 'OPC2', 'OPC3', 'OPC4', 'OPC5', 'Vivo estado neuro desconocido', 'Hemodinamica', 'Desconocido', 'Traslado', 'Repatriado'), AuxDataType::TYPE_EVOLUCION_6_MESES);
        $this->addAuxData(array('OPC1', 'OPC1 Buena capac', 'OPC2', 'OPC3', 'OPC4', 'OPC5', 'Vivo estado neuro desconocido', 'Desconocido', 'Repatriado'), AuxDataType::TYPE_EVOLUCION_ANIO);
        
        $this->em->flush();

    }

    public function getOrder()
    {
        return 20; //Ha de ejecutarse esta fixture después de la de AuxDataType
    }

    private function addAuxData(array $names, $auxTypeFixtureReference, $state=AuxData::STATE_ENABLED)
    {
        foreach ($names as $name) {
            $entity = new AuxData();
            $this->em->persist($entity);
            $entity->setType($this->getReference($auxTypeFixtureReference));
            $entity->setName($name);
            $entity->setState($state);
        }
    }

}
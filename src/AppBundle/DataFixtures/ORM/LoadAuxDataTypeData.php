<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\AuxDataType;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadAuxDataTypeData extends AbstractFixture implements OrderedFixtureInterface
{
    private $em;   //Doctrine

    public function load(ObjectManager $manager)
    {
        $this->em = $manager;

        /* PRIMER BLOQUE */
        $this->addAuxDataType('Registro PCR', AuxDataType::TYPE_REGISTROPCR);
        $this->addAuxDataType('Completo', AuxDataType::TYPE_COMPLETO);
        $this->addAuxDataType('Motivo de llamada', AuxDataType::TYPE_MOTIVOLLAMADA);
        $this->addAuxDataType('Unidad', AuxDataType::TYPE_UNIDAD);
        $this->addAuxDataType('Médico', AuxDataType::TYPE_MEDICO);
        $this->addAuxDataType('DUE', AuxDataType::TYPE_DUE);
        $this->addAuxDataType('Regulador', AuxDataType::TYPE_REGULADOR);
        $this->addAuxDataType('Población', AuxDataType::TYPE_POBLACION);
        $this->addAuxDataType('Sexo', AuxDataType::TYPE_SEXO);
        /* SEGUNDO BLOQUE */
        $this->addAuxDataType('HTA', AuxDataType::TYPE_HTA);
        $this->addAuxDataType('DMNI', AuxDataType::TYPE_DMNI);
        $this->addAuxDataType('Hipercolesterol', AuxDataType::TYPE_HIPERCOLESTEROL);
        $this->addAuxDataType('Fumador', AuxDataType::TYPE_FUMADOR);
        $this->addAuxDataType('Enolismo', AuxDataType::TYPE_ENOLISMO);
        $this->addAuxDataType('Cocaína', AuxDataType::TYPE_COCAINA);
        $this->addAuxDataType('HIV', AuxDataType::TYPE_HIV);
        $this->addAuxDataType('IAM', AuxDataType::TYPE_IAM);
        $this->addAuxDataType('DAI', AuxDataType::TYPE_DAI);
        $this->addAuxDataType('EPOC', AuxDataType::TYPE_EPOC);
        $this->addAuxDataType('AVC', AuxDataType::TYPE_AVC);
        $this->addAuxDataType('Ins Renal', AuxDataType::TYPE_INSRENAL);
        $this->addAuxDataType('Obesidad', AuxDataType::TYPE_OBESIDAD);
        $this->addAuxDataType('Neoplasia', AuxDataType::TYPE_NEOPLASIA);
        $this->addAuxDataType('Familia MS', AuxDataType::TYPE_FAMILIA_MS);
        $this->addAuxDataType('Familia CI', AuxDataType::TYPE_FAMILIA_CI);
        $this->addAuxDataType('Cir Cardíaca', AuxDataType::TYPE_CIRCARDIACA);
        /* TERCER BLOQUE */
        $this->addAuxDataType('Lugar PCR', AuxDataType::TYPE_LUGAR_PCR);
        $this->addAuxDataType('Presenciada', AuxDataType::TYPE_PRESENCIADA);
        $this->addAuxDataType('Quien', AuxDataType::TYPE_QUIEN);
        $this->addAuxDataType('Tipo primer RCP', AuxDataType::TYPE_TIPO_1ER_RCP);
        $this->addAuxDataType('Tiempo primer DSF', AuxDataType::TYPE_TIEMPO_1ER_DSF);
        $this->addAuxDataType('RCP telefónica', AuxDataType::TYPE_RCP_TELEFONICA);
        /* CUARTO BLOQUE */
        $this->addAuxDataType('Primer interviniente', AuxDataType::TYPE_1ER_INTERVINIENTE);
        $this->addAuxDataType('Presencia policía', AuxDataType::TYPE_PRESENCIA_POLICIA);
        $this->addAuxDataType('Tiempo RCPB', AuxDataType::TYPE_TIEMPO_RCPB);
        $this->addAuxDataType('DESA', AuxDataType::TYPE_DESA);
        $this->addAuxDataType('RCP avanzada', AuxDataType::TYPE_RCP_AVANZADA);
        $this->addAuxDataType('Tipo RCPA', AuxDataType::TYPE_TIPO_RCPA);
        $this->addAuxDataType('Ritmo inicial', AuxDataType::TYPE_RITMO_INICIAL);
        $this->addAuxDataType('Ritmo SI ROSC', AuxDataType::TYPE_RITMO_SI_ROSC);
        $this->addAuxDataType('Glasgow SI ROSC', AuxDataType::TYPE_GLASGOW_SI_ROSC);
        /* QUINTO BLOQUE */
        $this->addAuxDataType('Vía aérea', AuxDataType::TYPE_VIA_AEREA);
        $this->addAuxDataType('Acceso venoso', AuxDataType::TYPE_ACCESO_VENOSO);
        $this->addAuxDataType('Capnógrafo', AuxDataType::TYPE_CAPNOGRAFO);
        $this->addAuxDataType('Hipotermia', AuxDataType::TYPE_HIPOTERMIA);
        $this->addAuxDataType('Marcapasos', AuxDataType::TYPE_MARCAPASOS);
        $this->addAuxDataType('Trombólisis', AuxDataType::TYPE_TROMBOLISIS);
        $this->addAuxDataType('Drenaje torácico', AuxDataType::TYPE_DRENAJE_TORACICO);
        $this->addAuxDataType('Pericardiocentesis', AuxDataType::TYPE_PERICARDIOCENTESIS);
        $this->addAuxDataType('Adrenalina', AuxDataType::TYPE_ADRENALINA);
        $this->addAuxDataType('Atropina', AuxDataType::TYPE_ATROPINA);
        $this->addAuxDataType('Amiodarona', AuxDataType::TYPE_AMIODARONA);
        $this->addAuxDataType('Bicarbonato', AuxDataType::TYPE_BICARBONATO);
        $this->addAuxDataType('Drogas', AuxDataType::TYPE_DROGAS);
        $this->addAuxDataType('Electrolitos', AuxDataType::TYPE_ELECTROLITOS);
        /* SEXTO BLOQUE */
        $this->addAuxDataType('Etiologia', AuxDataType::TYPE_ETIOLOGIA);
        $this->addAuxDataType('Sospecha etiologia', AuxDataType::TYPE_SOSPECHA_ETIOLOGICA);
        $this->addAuxDataType('Exitus', AuxDataType::TYPE_EXITUS);
        $this->addAuxDataType('Hospital destino', AuxDataType::TYPE_HOSPITAL_DESTINO);
        $this->addAuxDataType('Hipotermia', AuxDataType::TYPE_HIPOTERMIA_HOSP);
        $this->addAuxDataType('DAI', AuxDataType::TYPE_DAI2);
        $this->addAuxDataType('Donante', AuxDataType::TYPE_DONANTE);
        $this->addAuxDataType('LET', AuxDataType::TYPE_LET);
        $this->addAuxDataType('Evolución alta', AuxDataType::TYPE_EVOLUCION_ALTA);
        $this->addAuxDataType('Evolución 6 meses', AuxDataType::TYPE_EVOLUCION_6_MESES);
        $this->addAuxDataType('Evolución año', AuxDataType::TYPE_EVOLUCION_ANIO);
        
        $this->em->flush();
    }

    public function getOrder()
    {
        return 10; // Esta fixture va antes de la de AuxData
    }

    private function addAuxDataType($desc, $type)
    {
        $entity = new AuxDataType();
        $this->em->persist($entity);
        $entity->setShortName($type);
        $entity->setDescription($desc);
        $this->addReference($type, $entity);
    }
}
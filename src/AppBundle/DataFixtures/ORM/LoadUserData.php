<?php

namespace AppBundle\DataFixtures\ORM;

use Bogo\Entity\User;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadUserData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $entity = new User();
        $entity->setUsername('admin');
        $entity->setUsernameCanonical('admin');
        $entity->setEmail('admin@bogo.ai');
        $entity->setEmailCanonical('admin@bogo.ai');
        $entity->setEnabled(true);
        $entity->setPlainPassword('admin');
        $entity->addRole('ROLE_ADMIN');

        $manager->persist($entity);
        $manager->flush();

        $entity = new User();
        $entity->setUsername('user');
        $entity->setEmail('user@bogo.ai');
        $entity->setEnabled(true);
        $entity->setPlainPassword('user');

        $manager->persist($entity);
        $manager->flush();
    }

    public function getOrder()
    {
        //return 7; // the order in which fixtures will be loaded
    }
}
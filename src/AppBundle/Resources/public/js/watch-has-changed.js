/**
 * Created by carlos on 21/04/15.
 */
// Written by Luke Morton, licensed under MIT
// https://gist.github.com/DrPheltRight/4131266

// Modificado por Carlos Testera
/**
 * Añadimos dos funciones utilizables como cualquier otra de jquery para poder
 * ver si un elemente (normalmente un form) ha cambiado. En el ciclo de cambios
 * hay que llamar a watchChanges, lo cual te da el "estado" del elemento en ese
 * momento, y luego hasChanged comprueba si dicho estado ha cambiado.
 */
$(function() {
    $.fn.extend({
        watchChanges: function () {
            return this.each(function () {
                $.data(this, 'formHash', $(this).serialize());
            });
        },
        hasChanged: function () {
            var hasChanged = false;
            this.each(function () {
                var formHash = $.data(this, 'formHash');

                if (formHash != null && formHash !== $(this).serialize()) {
                    hasChanged = true;
                    return false;
                }
            });
            return hasChanged;
        }
    });
});

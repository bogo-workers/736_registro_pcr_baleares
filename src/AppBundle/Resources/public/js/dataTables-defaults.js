/**
 * Proporciona opciones por defecto para nuestros dataTables de backnet
 */

$(function() {

    // Opciones por defecto para todos los dataTables
    $.extend(true, $.fn.dataTable.defaults, {
        /*
        'stateSave':       true, // Guardar el estado de página y filtros entre requests por medio de cookies
        'autoWidth':       true,
        'pageLength':      10,   // Número de filas por página por defecto
        'lengthChange':    true,
        //'lengthMenu':      [[5, 10, 25, 50, 100, -1], [5, 10, 25, 50, 100, Translator.trans('Todos')]],
        'processing':      true, // Mostrar barra de 'procesando' cuando está ocupado
        'deferRender':     true,
        */
        'language': {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"  //Si funciona esto, lo ideal
            //"url": "datatables_spanish.txt"   //Sino, lo tenemos en local
        }
    });

});

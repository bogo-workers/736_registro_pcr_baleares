/**
 * Created by carlos on 23/03/15.
 */

var TableAuxDataList = function() {

    /* Esta función obtiene el tipo de dato auxiliar a partir de la url. Así sabemos que listado generar. */
    var getAuxDataType = function() {
        var currentUrl = document.URL;
        var type = currentUrl.split("/").pop();

        return type;
    }

    /* Esta función genera el listado mediante datatables. Recibe el tipo de dato auxiliar que se usa en la petición ajax que rellena la tabla */
    var handleTable = function(auxDataType) {
        $('#table_auxdata_list').DataTable({
            'columnDefs': [
                {
                    'targets': 0,
                    'orderable': false,
                    'render': function(data, type, row) {
                        //var editLink = '<a href="'+Routing.generate('bogo_backnet_auxdata_edit', { id: data })+'" class="edit" title="'+ Translator.trans('Editar') +'"><i class="fa fa-edit"></i></a>';
                        //var deleteLink = '<a href="'+Routing.generate('bogo_backnet_auxdata_delete', { id: data })+'" class="delete" title="'+ Translator.trans('Eliminar') +'"><i class="fa fa-trash-o"></i></a>';
                        var editLink = '<a class="btn Default btn-xs green" href="'+Routing.generate('bogo_backnet_auxdata_edit', { id: data })+'"><i class="fa fa-pencil"></i> '+ Translator.trans('Editar') +' </a>';
                        var deleteLink = '<a class="btn Default btn-xs black" href="'+Routing.generate('bogo_backnet_auxdata_delete', { id: data })+'"><i class="fa fa-trash-o"></i> '+ Translator.trans('Eliminar') +' </a>';
                        return editLink + ' ' + deleteLink;

                    },
                    'class': 'text-center',
                    'width': '150px'
                },
                {
                    // state, active (V o X)
                    'targets': 2,
                    'orderable': false,
                    'render': function(data) {
                        return data ? '<i class="fa fa-check"></i>' : '<i class="fa fa-times"></i>';
                    }
                },
            ],
            'columns':[
                {'data': 'id'},
                {'data': 'name'},
                {'data': 'active'}
            ],
            'stateSave': true,  //Para que esto funcione, el id de la tabla ha de ser único
            'order': [ [1, 'asc'] ], // set first column as a Default sort by asc
            'ajax': Routing.generate('bogo_backnet_auxdata_list', {type: auxDataType}) // ruta de FOSJsRoutingBundle,
        });
    }

    var handleSelect2 = function() {
        $('select').select2({
            placeholder: Translator.trans("Cambiar a listado de ...")
        });
    }

    /**
     * Cuando seleccionamos una opción de la combo de ir a otro listado, redireccionamos al
     * tipo de dato auxiliar indicado en la combo
     */
    var handleAuxTypesSelect = function() {
        $('#select_jump2auxType').change(function(e) {
            var route = Routing.generate('bogo_backnet_auxdata_list', {type: $(this).val()});
            window.location.href = route;
        });
    }

    return {
        //main function to initiate the module
        init: function() {
            handleTable(getAuxDataType());
            handleSelect2();
            handleAuxTypesSelect();
        }
    };

}();

$(function() {
    TableAuxDataList.init();
});

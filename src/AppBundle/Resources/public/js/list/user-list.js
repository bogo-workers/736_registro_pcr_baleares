/**
 * Created by carlos on 23/03/15.
 */

var TableUserList = function() {

    /* Esta función genera el listado mediante datatables. */
    var handleTable = function() {
        $('#table_user_list').DataTable({
            'columnDefs': [
                {
                    'targets': 0,
                    'orderable': false,
                    'render': function(data, type, row) {
                        var editLink = '<a class="btn Default btn-xs green" href="'+Routing.generate('bogo_users_edit', { id: data })+'"><i class="fa fa-pencil"></i> '+ Translator.trans('Editar') +' </a>';
                        var deleteLink = '<a class="btn Default btn-xs black" href="'+Routing.generate('bogo_users_delete', { id: data })+'"><i class="fa fa-trash-o"></i> '+ Translator.trans('Eliminar') +' </a>';
                        return editLink + ' ' + deleteLink;

                    },
                    'class': 'text-center',
                    'width': '150px'
                },
                {
                    // state, active (V o X)
                    'targets': 3,
                    'orderable': false,
                    'render': function(data) {
                        return data ? '<i class="fa fa-check"></i>' : '<i class="fa fa-times"></i>';
                    }
                },
            ],
            'columns':[
                {'data': 'id'},
                {'data': 'email'},
                {'data': 'rol'},
                {'data': 'enabled'}
            ],
            'stateSave': true,  //Para que esto funcione, el id de la tabla ha de ser único
            'order': [ [1, 'asc'] ], // set first column as a Default sort by asc
            'ajax': Routing.generate('bogo_users_list') // ruta de FOSJsRoutingBundle,
        });
    }

    return {
        //main function to initiate the module
        init: function() {
            handleTable();
        }
    };

}();

$(function() {
    TableUserList.init();
});

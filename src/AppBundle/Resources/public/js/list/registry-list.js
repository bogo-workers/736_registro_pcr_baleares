/**
 * Created by carlos on 23/03/15.
 */

var TableRegistryList = function() {

    /* Esta función genera el listado mediante datatables. */
    var handleTable = function() {
        $('#table_registry_list').DataTable({
            'columnDefs': [
                {
                    'targets': 0,
                    'orderable': false,
                    'render': function(data, type, row) {
                        var editLink = '<a class="btn default btn-xs green" href="'+Routing.generate('bogo_backnet_registry_edit', { id: data })+'"><i class="fa fa-pencil"></i> '+ Translator.trans('Editar') +' </a>';
                        var deleteLink = '<a class="btn default btn-xs black" href="'+Routing.generate('bogo_backnet_registry_delete', { id: data })+'"><i class="fa fa-trash-o"></i> '+ Translator.trans('Eliminar') +' </a>';
                        return editLink + ' ' + deleteLink;

                    },
                    'class': 'text-center',
                    'width': '150px'
                },
            ],
            'columns':[
                {'data': 'id'},
                {'data': 'name'},
                {'data': 'date'},
                {'data': 'age'},
                {'data': 'serviceCode'},
                {'data': 'serviceNumber'}
            ],
            'stateSave': true,  //Para que esto funcione, el id de la tabla ha de ser único
            'order': [ [2, 'desc'] ], // set second column as a Default sort by asc
            'ajax': Routing.generate('bogo_backnet_registry_list') // ruta de FOSJsRoutingBundle,
        });
    };

    /* Esta función asocia el manejador del botón de resetear los filtros a la función que lo hace */
    var handleClearListFiltersButton = function() {
        $("#clear_list_filters").click(function() {
            $("#table_registry_list").DataTable().state.clear();    //Borramos filtros (datatables 1.10.1 y superior)
            window.location.reload();   //Recargamos
        });
    };

    return {
        //main function to initiate the module
        init: function() {
            handleTable();
            handleClearListFiltersButton();
        }
    };
}();

$(function() {
    TableRegistryList.init();
});

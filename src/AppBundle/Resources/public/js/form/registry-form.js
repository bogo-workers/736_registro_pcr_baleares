/**
 * Created by carlos on 05/04/15.
 */

var form = null;    //Hacemos referencia al form en varios sitios, así que en el init lo apunto desde esta variable

var RegistryForm = function() {

    /**
     * Esto gestiona el blockUI. Debería ser tan fácil como lo que está comentado,
     * pero hay un fallo en Metronic con una función (getGlobalImgPath()) que hace
     * que no funcione.
     * Así que tengo que tener el div en la plantilla del formulario y pasárselo
     * al blockUI como mensaje a mostrar.
     *
     * Por supuesto, esto hecho así hace que que blockUI funcione para las peticiones
     * ajax. En el form de registros me sirve con eso.
     */
    var handleBlockUI = function() {
        //$(this).ajaxStart(Metronic.blockUI).ajaxStop(Metronic.unblockUI);
        $.blockUI.defaults.message = $('#savingChangesMessage');
        $(this).ajaxStart($.blockUI).ajaxStop($.unblockUI);
    };

    var handleToastr = function() {
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "positionClass": "toast-top-right",
            "onclick": null,
            "showDuration": "1000",
            "hideDuration": "1000",
            "timeOut": "3000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }
    };

    var handleSelect2 = function() {
        $('select').select2({
            placeholder: Translator.trans("Seleccione un elemento")
        });
    };

    var handleDatepicker = function() {
        $('.form_datetime').datepicker({
            autoclose: true
        });
    };

    var handleTimepicker = function() {
        $('.timepicker-24').timepicker({
            minuteStep: 5,
            showMeridian: false,
            defaultTime: false
        });
    };


    /**
     * Envía el formulario por POST
     */
    var postForm = function() {
        var values = {};
        $.each( form.serializeArray(), function(i, field) {
            values[field.name] = field.value;
        });
        var entityId = values['backnet_registry_form[id]'];

        $.ajax({
            type        : 'POST',
            url         : Routing.generate('bogo_backnet_registry_edit', {id: entityId}),
            data        : values,
            success     : function(data) {
                toastr['success'](data, Translator.trans("OPERACIÓN CORRECTA"));
                formWatcher = form.watchChanges();  //Vuelvo a guardar el estado actual del form para que no se marque como modificado
            },
            error       : function(data) {
                toastr['error'](data['responseText'], Translator.trans('ERROR'));
            }
        });
    };

    /**
     * Calcula la diferencia entre la hora de primer RCP y la hora PCR
     * @returns {*}
     */
    var calculateTiempoDemora = function() {
        //Función que me devuelve un intervalo en ms en formato hora,min,sec,ms
        var msToTime = function(duration) {
            var milliseconds = parseInt((duration%1000)/100)
                , seconds = parseInt((duration/1000)%60)
                , minutes = parseInt((duration/(1000*60))%60)
                , hours = parseInt((duration/(1000*60*60))%24);

            hours = (hours < 10) ? "0" + hours : hours;
            minutes = (minutes < 10) ? "0" + minutes : minutes;
            seconds = (seconds < 10) ? "0" + seconds : seconds;

            return {
                'hours': hours,
                'minutes': minutes,
                'seconds': seconds,
                'milliseconds': milliseconds
            };
        };

        var fakeDate = "1/1/2001 "; //Fecha cualquiera para trabajar con objetos Date
        var horaPCR = new Date(fakeDate + $("#backnet_registry_form_horaPcr").val());   //HoraPCR en formato Date
        var horaPrimerRCP = new Date(fakeDate + $("#backnet_registry_form_horaPrimerRcp").val());   //HoraPrimerRCp en formato Date

        //Si son fechas válidas
        if (!isNaN(Date.parse(horaPCR)) && !isNaN(Date.parse(horaPrimerRCP))) {
            var diffInMs = Math.abs(horaPrimerRCP - horaPCR);   //Diferencia en ms
            var diff = msToTime(diffInMs);  //Obtengo horas y minutos
            return (diff.hours+":"+diff.minutes);   //Lo devuelvo
        }
        return null;
    };


    return {
        //main function to initiate the module
        init: function() {
            form = $("form[name='backnet_registry_form']"); //Form. Lo usamos en varios sitios
            handleBlockUI();
            handleToastr();
            handleDatepicker();
            handleTimepicker();
            handleSelect2();
        },
        saveChanges: function() {
            postForm();
        },
        updateTiempoDemoraField: function() {
            var t = calculateTiempoDemora();
            if (t != null) {
                $("#backnet_registry_form_tiempoDemora").val(t);
            }
        }
    };

}();


$(function() {
    RegistryForm.init();
    var formWatcher = form.watchChanges();  //Al cargar ya guardo el estado del form

    //Cada vez que cambiemos de pestaña, guardamos cambios
    $(".tabbable > ul a").click(function(e) {
        //El evento se lanza incluso si pinchamos en la pestaña activa (cosa que no queremos), así que lo comprobamos
        //También comprobamos que el formulario ha cambiado, sino no lanzamos el evento (usamos watch-has-changed.js)
        if (formWatcher.hasChanged() && !$(this).parent().hasClass("active")) {
            $("#backnet_registry_form_submit").trigger("click");  //Si ha habido cambios, hacemos submit del form
            //OJO. Dejar el evento "click" al botón del form. No usar "submit" del form, porque sino no se lanza la validación de cliente
        }
    });

    //Controlamos el envío del form. Si estamos editando paramos el submit y lanzamos la petición ajax
    form.submit(function(e) {
        //Solamente ha de funcionar así si estamos en la edición (tenemos valor en el campo id y no están deshabilitados los campos)
        if ($("#backnet_registry_form_id").val() && $("#backnet_registry_form_nombre").attr("disabled") != "disabled") {
            e.preventDefault();
            RegistryForm.saveChanges();
        }
    });

    //Controlamos los cambios en las horas de los campos de hora pcr y hora primer rcp para calcular la diferencia y pasarlo a tiempo demora
    $("#backnet_registry_form_horaPcr").change(function(e) {
        RegistryForm.updateTiempoDemoraField();
    });
    $("#backnet_registry_form_horaPrimerRcp").change(function(e) {
        RegistryForm.updateTiempoDemoraField();
    });

});

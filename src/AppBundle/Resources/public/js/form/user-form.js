/**
 * Created by carlos on 26/04/15.
 */

var UserForm = function() {


    var handleSelect2 = function() {
        $('select').select2({});
    }


    return {
        //main function to initiate the module
        init: function() {
            handleSelect2();
        }
    };

}();


$(function() {
    UserForm.init();
});

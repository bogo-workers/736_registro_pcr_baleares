<?php

namespace Bogo\Entity;

use Bogo\Entity\Traits\TimestampableEntity;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * 'user' is a reserved keyword in SQL so you cannot use it as table name.
 *
 * @ORM\Table(name="users")
 * @ORM\Entity(repositoryClass="Bogo\Entity\Repository\UserRepository")
 */
class User extends BaseUser
{
    /**
     * Timestampable behavior
     */
    use TimestampableEntity;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToOne(targetEntity="UserProfile", cascade={"all"})
     **/
    private $profile;

    /**
     * @ORM\OneToOne(targetEntity="UserContactInfo", cascade={"all"})
     **/
    private $contactInfo;

    /**
     * @ORM\OneToOne(targetEntity="UserSocial", cascade={"all"})
     **/
    private $social;


    /**
     * {@inheritDoc}
     */
    public function __construct()
    {
        parent::__construct();
        // your own logic
    }



    /**
     * Set id
     *
     * @param int $id
     * @return User
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set social
     *
     * @param \Bogo\Entity\UserSocial $social
     * @return User
     */
    public function setSocial(UserSocial $social = null)
    {
        $this->social = $social;

        return $this;
    }

    /**
     * Get social
     *
     * @return \Bogo\Entity\UserSocial
     */
    public function getSocial()
    {
        return $this->social;
    }

    /**
     * Set profile
     *
     * @param \Bogo\Entity\UserProfile $profile
     * @return User
     */
    public function setProfile(UserProfile $profile = null)
    {
        $this->profile = $profile;

        return $this;
    }

    /**
     * Get profile
     *
     * @return \Bogo\Entity\UserProfile
     */
    public function getProfile()
    {
        return $this->profile;
    }

    /**
     * Set contactInfo
     *
     * @param \Bogo\Entity\UserContactInfo $contactInfo
     * @return User
     */
    public function setContactInfo(UserContactInfo $contactInfo = null)
    {
        $this->contactInfo = $contactInfo;

        return $this;
    }

    /**
     * Get contactInfo
     *
     * @return \Bogo\Entity\UserContactInfo
     */
    public function getContactInfo()
    {
        return $this->contactInfo;
    }


    /**
     * Get fullName
     * Useful for the view templates, so that we can use this method like this: {{ app.user.fullName }}
     *
     * @return string
     */
    public function getFullName()
    {
        //return $this->name . ' ' . $this->surname1 . (!empty($this->surname2) ? ' ' . $this->surname2 : '');
    }


    /**
     * ----------------------------------------------------------------------------------------------------------------
     * Métodos auxiliares
     * ----------------------------------------------------------------------------------------------------------------
     */

    /**
     * __toString()
     * @return string
     */
    public function __toString()
    {
        return $this->getEmail();
    }

    /**
     * toArray()
     *
     * Método para transformar una entidad en un array listo para enchufárselo a una response en json.
     *
     * Útil para devolver resultados paginados a los dataTables, para no tener que componer los arrays en los
     * controladores
     *
     * @return array
     */
    public function toArray()
    {
        return array(
            'id' => $this->getId(),
            'email' => $this->getEmail(),
            'rol' => $this->getRoles()[0],
            'enabled' => $this->isEnabled(),
        );
    }

}

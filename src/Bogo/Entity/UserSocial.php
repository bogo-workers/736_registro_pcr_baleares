<?php
/**
 * Created by PhpStorm.
 * User: carlos
 * Date: 27/04/15
 * Time: 11:04
 */

namespace Bogo\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="user_social")
 *
 * Class UserSocial
 * @package AppBundle\Entity
 */
class UserSocial
{
    /**
     * @var int|null
     *
     * @ORM\Id
     * @ORM\Column(type="bigint", options={"unsigned":true})
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $facebookUid;
    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $facebookName;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $twitterUid;
    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $twitterName;
    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set facebookUid
     *
     * @param string $facebookUid
     * @return UserSocial
     */
    public function setFacebookUid($facebookUid)
    {
        $this->facebookUid = $facebookUid;

        return $this;
    }

    /**
     * Get facebookUid
     *
     * @return string 
     */
    public function getFacebookUid()
    {
        return $this->facebookUid;
    }

    /**
     * Set facebookName
     *
     * @param string $facebookName
     * @return UserSocial
     */
    public function setFacebookName($facebookName)
    {
        $this->facebookName = $facebookName;

        return $this;
    }

    /**
     * Get facebookName
     *
     * @return string 
     */
    public function getFacebookName()
    {
        return $this->facebookName;
    }

    /**
     * Set twitterUid
     *
     * @param string $twitterUid
     * @return UserSocial
     */
    public function setTwitterUid($twitterUid)
    {
        $this->twitterUid = $twitterUid;

        return $this;
    }

    /**
     * Get twitterUid
     *
     * @return string 
     */
    public function getTwitterUid()
    {
        return $this->twitterUid;
    }

    /**
     * Set twitterName
     *
     * @param string $twitterName
     * @return UserSocial
     */
    public function setTwitterName($twitterName)
    {
        $this->twitterName = $twitterName;

        return $this;
    }

    /**
     * Get twitterName
     *
     * @return string 
     */
    public function getTwitterName()
    {
        return $this->twitterName;
    }

}

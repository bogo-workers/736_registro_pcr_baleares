<?php

namespace Bogo\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

/**
 * BaseRepository
 *
 * Common methods for repositories
 */
abstract class BaseRepository extends EntityRepository
{

    public function findForDataTables($criteria=null)
    {
        if (is_null($criteria))
            $entities = $this->findAll();
        else
            $entities = $this->findBy($criteria);
        $data = array();
        foreach ($entities as $e) {
            $data[] = $e->toArray();
        }
        $output = array(
            'data' => $data,
        );

        return $output;
    }

}
CHANGELOG - 736_registro_pcr_baleares
=====================================

20151013_02
-----------

* Unificar OPCs. No implica cambios en el código, solamente a nivel de base de datos. Resumen de como lo he hecho:

```sql
/* OPCs para evolución al alta */
select * from auxdata where type_id = (select id from auxdatatypes where shortName='evolucionalta');
/* OPCs para evolución a los 6 meses */
select * from auxdata where type_id = (select id from auxdatatypes where shortName='evolucion6meses');
/* OPCs para evolución al año */
select * from auxdata where type_id = (select id from auxdatatypes where shortName='evolucionanio');

/* Para cada par de OPCs "duplicadas". Esto se repite para cada par de OPCs duplicadas en cada una de las evoluciones */
set @toDelete:=id_opc_a_eliminar;
set @toKeep:=id_opc_a_mantener;

/* Comprobaciones (se puede omitir) */
select * from registries where id in (select registry_id from registry_auxdata where auxdata_id=@toDelete);
select * from registries where id in (select registry_id from registry_auxdata where auxdata_id=@toKeep);

/* Actualizamos tabla de relaciones de registros con OPCs */
update registry_auxdata set auxdata_id=@toKeep where auxdata_id=@toDelete;
```

20151013_01
-----------

* Cambiar nombres de las pestañas de los bloques del formulario de registros.
* En el bloque de antecedentes, todos los campos han de tener el valor "DESCONOCIDO" como valor por defecto.
* Cálculo automático del valor del campo "Tiempo Demora" de bloque localización. Se actualiza al dar valores a los campos
de "Hora PCR" y "Hora Primer RCP".

---

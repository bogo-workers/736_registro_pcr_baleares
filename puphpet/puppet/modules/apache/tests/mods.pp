## Default mods

# Base class. Declares Default vhost on port 80 and Default ssl
# vhost on port 443 listening on all interfaces and serving
# $apache::docroot, and declaring our Default set of modules.
class { 'apache':
  default_mods => true,
}


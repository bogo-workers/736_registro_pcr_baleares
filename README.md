736_registro_pcr_baleares
=========================

## Ejecución en entorno de desarrollo

Una vez que tengamos la aplicación funcionando en entorno de desarrollo (ver los pasos siguientes), la aplicación queda
accesible (si se usa vagrant) en:

`http://bogo_736_pcr.dev/app_dev.php/backnet`

Hay dos usuarios (usuario/password):
* `user@bogo.ai/user`: Usuario autenticado en backnet
* `admin@bogo.ai/admin`: Usuario autenticado en backnet con rol de administrador

## Obtener proyecto

*RECORDAR*, trabajamos *SIEMPRE* en la rama `develop`. *NO* tocar la rama `master`.

Si no tenéis aún una copia del repositorio en vuestra máquina de desarrollo, hay que clonarlo y luego hacer un pull de
la rama develop.

```
$ git clone git@gitlab.com:bogo-workers/736_registro_pcr_baleares.git   #Clono el repositorio
$ cd 736_registro_pcr_baleares                                          
$ git checkout -b develop                                               #Creo una nueva rama develop y me cambio a ella
$ git pull --rebase origin develop                                      #Obtengo la rama remota develop en la mía
```

Una vez que lo tengáis, ya podéis gestionarlo con vuestra herramienta (sourcetree, plugin de eclipse, lo que sea).

NOTA: Si se quiere evitar la línea de comandos, el proceso de clonado y pull anterior se puede hacer directamente con 
el IDE que utilicéis. Cada uno con el suyo tendría que mirar como.

## Puesta en marcha en entorno desarrollo

Ver en proyecto Bogo_Doc como tener funcionando vagrant y composer.

Levantar la máquina vagrant asociada al proyecto y conectarse a ella. Si es la primera vez, estos son los comandos
necesarios para que el entorno de desarrollo quede operativo:

```
$ cd /var/www/project
$ php composer.phar self-update
$ php composer.phar update  --prefer-dist   #Recordar que estoy hay que hacerlo regularmente, y siempre si se añaden/modifican módulos

$ php app/console assets:install --symlink
$ php app/console bazinga:js-translation:dump
$ php app/console assetic:dump

$ php app/console doctrine:database:create  #Si ya existe la base de datos no es necesario
$ php app/console doctrine:schema:update --force
$ php app/console doctrine:fixtures:load
```

## Extracción de literales para traducción

Cuando queremos extraer los términos a traducir de nuestro código, tenemos que invocar al extractor. Mirar la ayuda
de la opción `translation`, pero lo más usado especificar una configuración específica de las que tengamos en el
fichero `config.yml` o especificar el nombre del bundle. En resumen:

```
$ php app/console translation:extract [--config=config_from_config.yml] [--bundle=bundle_name] locale(s)
```

Por ejemplo:

```
$ php app/console translation:extract --config=app en es
...

$ php app/console translation:extract --bundle=BogoBacknetBundle en es
...
```

## Limpiar cache

Después de los procesos anteriores, cuando algo no funcione sospechosamente o después de algún cambio en assets y carga
de librerías, etc, puede ser conveniente limpiar la cache de symfony:

```
$ php app/console cache:clear
```

